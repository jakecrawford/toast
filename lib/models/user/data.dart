import 'package:flutter/material.dart';
import 'package:toast/models/friend/data.dart';
import 'package:toast/models/friend/data.dart';

class UserData {
  String name;
  String uid;
  List<String> projects = [];
  List<FriendData> friends = [];
  
  UserData({
    @required this.name, 
    @required this.uid,
    @required this.friends,
    this.projects = const [],
  });

  UserData.fromJson(Map<String, dynamic> json) {
    this.name = json['name'] as String;
    this.uid = json['uid'] as String;
    this.friends =   (json['friends'] as List)?.map((e) => new FriendData.fromJson(e))?.toList(); //json['friends'] == null ? null: (json['friends'] as List);
    this.projects = (json['projects'] as List)?.map((e) => e as String)?.toList();
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    'name': this.name,
    'uid': this.uid,
    'projects': this.projects,
    'friends': this.friends.map((e) => e.toJson()).toList(),
  };
}
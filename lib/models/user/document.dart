import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:toast/models/user/data.dart';

class UserDocument {
  String id;
  UserData profile;

  UserDocument.fromSnapshot(DocumentSnapshot snapshot){
    this.id = snapshot.id;
    this.profile = new UserData.fromJson(snapshot.data());
  }

  UserDocument({
    @required this.id, @required this.profile,
  });
}
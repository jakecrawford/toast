import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
part 'data.g.dart';

@JsonSerializable()
class MemberData {
  String name;
  String uid;
  bool approved;

  MemberData({
    @required this.name, 
    @required this.uid, 
    @required this.approved,
  });

  factory MemberData.fromJson(Map<String, dynamic> json) => _$MemberDataFromJson(json);

  Map<String, dynamic> toJson() => _$MemberDataToJson(this);
}
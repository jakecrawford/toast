
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:toast/models/member/data.dart';

class MemberDocument {
  MemberData memberData;
  String id;

  MemberDocument.fromQuerySnapshot(QueryDocumentSnapshot snapshot){
    this.id = snapshot.id;
    this.memberData = new MemberData.fromJson(snapshot.data());
  }

  MemberDocument({
    @required this.id, @required this.memberData
  });
}
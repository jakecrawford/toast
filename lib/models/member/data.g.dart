// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MemberData _$MemberDataFromJson(Map<String, dynamic> json) {
  return MemberData(
    name: json['name'] as String,
    uid: json['uid'] as String,
    approved: json['approved'] as bool,
  );
}

Map<String, dynamic> _$MemberDataToJson(MemberData instance) =>
    <String, dynamic>{
      'name': instance.name,
      'uid': instance.uid,
      'approved': instance.approved,
    };

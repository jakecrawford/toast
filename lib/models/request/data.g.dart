// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RequestData _$RequestDataFromJson(Map<String, dynamic> json) {
  return RequestData(
    name: json['name'] as String,
    type: json['type'] as String,
    receiver: json['receiver'] as String,
    sender: json['sender'] as String,
    identifier: json['identifier'] as String,
  );
}

Map<String, dynamic> _$RequestDataToJson(RequestData instance) =>
    <String, dynamic>{
      'name': instance.name,
      'type': instance.type,
      'receiver': instance.receiver,
      'sender': instance.sender,
      'identifier': instance.identifier,
    };

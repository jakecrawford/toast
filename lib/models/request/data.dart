import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
part 'data.g.dart';

@JsonSerializable()
class RequestData {
  String name;
  String type;
  String receiver;
  String sender;
  String identifier;

  RequestData({
    @required this.name, @required this.type, @required this.receiver, @required this.sender, @required this.identifier
  });

  factory RequestData.fromJson(Map<String, dynamic> json) => _$RequestDataFromJson(json);

  Map<String, dynamic> toJson() => _$RequestDataToJson(this);
}
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:toast/models/project/data.dart';
import 'package:toast/models/request/data.dart';

class RequestDocument {
  String id;
  RequestData data;

  RequestDocument.fromSnapshot(DocumentSnapshot snapshot){
    this.id = snapshot.id;
    this.data = new RequestData.fromJson(snapshot.data());
  }

  RequestDocument.fromQuerySnapshot(QueryDocumentSnapshot snapshot){
    this.id = snapshot.id;
    this.data = new RequestData.fromJson(snapshot.data());
  }

  RequestDocument({
    @required this.data, @required this.id
  });
}
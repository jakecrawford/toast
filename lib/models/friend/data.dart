import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
part 'data.g.dart';

@JsonSerializable()
class FriendData {
  String name;
  String uid;

  FriendData({
    @required this.name, @required this.uid,
  });

  factory FriendData.fromJson(Map<String, dynamic> json) => _$FriendDataFromJson(json);

  Map<String, dynamic> toJson() => _$FriendDataToJson(this);
}
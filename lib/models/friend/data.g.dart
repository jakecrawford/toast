// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FriendData _$FriendDataFromJson(Map<String, dynamic> json) {
  return FriendData(
    name: json['name'] as String,
    uid: json['uid'] as String,
  );
}

Map<String, dynamic> _$FriendDataToJson(FriendData instance) =>
    <String, dynamic>{
      'name': instance.name,
      'uid': instance.uid,
    };

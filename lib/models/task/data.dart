import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
part 'data.g.dart';

@JsonSerializable()
class TaskData {
  String task;
  String user;
  String priority;
  String progress;

  TaskData({
    @required this.priority, 
    @required this.progress, 
    @required this.user, 
    @required this.task, 
  });

  factory TaskData.fromJson(Map<String, dynamic> json) => _$TaskDataFromJson(json);

  Map<String, dynamic> toJson() => _$TaskDataToJson(this);
}
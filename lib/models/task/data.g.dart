// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskData _$TaskDataFromJson(Map<String, dynamic> json) {
  return TaskData(
    priority: json['priority'] as String,
    progress: json['progress'] as String,
    user: json['user'] as String,
    task: json['task'] as String,
  );
}

Map<String, dynamic> _$TaskDataToJson(TaskData instance) => <String, dynamic>{
      'task': instance.task,
      'user': instance.user,
      'priority': instance.priority,
      'progress': instance.progress,
    };

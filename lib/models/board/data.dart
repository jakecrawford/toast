import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:toast/models/task/data.dart';

class BoardData {
  String title = "";
  String subtitle = "";
  List<TaskData> tasks = [];

  BoardData.fromQueryDocumentSnapshot(QueryDocumentSnapshot queryDocumentSnapshot){
    Map map = queryDocumentSnapshot.data();
    this.title = map['title'] ?? "";
    this.subtitle = map['subtitle'] ?? "";
    try {
      List<dynamic> temp = jsonDecode(map['tasks']);
      tasks = temp.map((data) => new TaskData.fromJson(data)).toList();
    } catch(e){

    }
  }

  BoardData.fromSnapshot(DocumentSnapshot snapshot){
    Map map = snapshot.data();
    this.title = map['title'] ?? "";
    this.subtitle = map['subtitle'] ?? "";
    try {
      List<dynamic> temp = jsonDecode(map['tasks']);
      tasks = temp.map((data) => new TaskData.fromJson(data)).toList();
    } catch(e){ 

    }
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    'title': this.title,
    'subtitle': this.subtitle,
    'tasks': jsonEncode(this.tasks.map((e) => e.toJson()).toList())
  };

  BoardData({
    @required this.title, 
    @required this.subtitle, 
    this.tasks,
  });
}
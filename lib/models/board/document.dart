import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:toast/models/board/data.dart';

class BoardDocument {
  String id;
  BoardData data;

  BoardDocument.fromQueryDocumentSnapshot(QueryDocumentSnapshot queryDocumentSnapshot){
    this.id = queryDocumentSnapshot.id;
    this.data = new BoardData.fromQueryDocumentSnapshot(queryDocumentSnapshot);
  }

  BoardDocument.fromSnapshot(DocumentSnapshot snapshot){
    this.id = snapshot.id;
    this.data = new BoardData.fromSnapshot(snapshot);
  }

  BoardDocument({
    @required this.id, @required this.data
  });
}
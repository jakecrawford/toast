import 'package:flutter/material.dart';
import 'package:toast/models/board/document.dart';
import 'package:json_annotation/json_annotation.dart';
part 'data.g.dart';

@JsonSerializable()
class ProjectData{
  String owner;
  String name;
  String primaryBoard;
  int iconIndex;
  int colorIndex;
  DateTime createdOn;
  
  ProjectData({
    this.name, 
    this.owner,
    this.createdOn,
    this.iconIndex,
    this.colorIndex,
    this.primaryBoard
  });

  factory ProjectData.fromJson(Map<String, dynamic> json) => _$ProjectDataFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectDataToJson(this);
}
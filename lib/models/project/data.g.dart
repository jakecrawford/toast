// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProjectData _$ProjectDataFromJson(Map<String, dynamic> json) {
  return ProjectData(
    name: json['name'] as String,
    owner: json['owner'] as String,
    createdOn: json['createdOn'] == null
        ? null
        : DateTime.parse(json['createdOn'] as String),
    iconIndex: json['iconIndex'] as int,
    colorIndex: json['colorIndex'] as int,
    primaryBoard: json['primaryBoard'] as String,
  );
}

Map<String, dynamic> _$ProjectDataToJson(ProjectData instance) =>
    <String, dynamic>{
      'owner': instance.owner,
      'name': instance.name,
      'primaryBoard': instance.primaryBoard,
      'iconIndex': instance.iconIndex,
      'colorIndex': instance.colorIndex,
      'createdOn': instance.createdOn?.toIso8601String(),
    };

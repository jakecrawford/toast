import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:toast/models/project/data.dart';

class ProjectDocument {
  String id;
  ProjectData data;

  ProjectDocument.fromSnapshot(DocumentSnapshot snapshot){
    this.id = snapshot.id;
    this.data = new ProjectData.fromJson(snapshot.data());
  }

  ProjectDocument({
    @required this.data, @required this.id
  });
}
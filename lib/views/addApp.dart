
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:toast/models/project/data.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/picker/colorPicker.dart';
import 'package:toast/toast/picker/iconPicker.dart';
import 'package:toast/toast/picker/list.dart';
import 'package:toast/views/dashboard.dart';
import 'package:toast/widgets/buttons/rowButton.dart';

class AddApp extends StatefulWidget {

  @override
  _AddAppState createState() => _AddAppState();
}

class _AddAppState extends State<AddApp> {

  String name;
  String version = "1.0.0";
  int color = 0;
  int icon = 0;

  bool creating = false;
  bool showVersionWarning = false;
  bool showNameWarning = false;

  bool verifyVersion(String v){
    try {
      RegExp versionExp = RegExp(r'(\d+\.)(\d+\.)(\d+)'); 
      String test = versionExp.stringMatch(v);
      if (test.length == v.length){
        return true;
      }
      return false;
    } catch(e){
      return false;
    }
  }

  addApp() async {
    try {
      bool valid = true;
      if (name == null || name == ""){
        valid = false;
        showNameWarning = true;
      } else {
        showNameWarning = false;
      } 
      if (!verifyVersion(version)){
        valid = false;
        showVersionWarning = true;
      } else {
        showVersionWarning = false;
      }

      if (!valid){
        setState(() {
          
        });
        return;
      }
      ProjectData projectData = new ProjectData(
        name: name,
        createdOn: DateTime.now(),
        iconIndex: icon,
        colorIndex: color,
        owner: FirebaseAuth.instance.currentUser.uid,
      );
      setState(() {
        creating = true;
      });
      await UserRepository.createProject(projectData, version);
      print("created projec t here");
      await Future.delayed(new Duration(seconds: 1));
      Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (context) => new DashboardView(),), 
        (Route<dynamic> route) => false
      );
    } catch(e){
      print("error creating");
      setState(() {
        creating = false;
      });
      print(e.toString());
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.transparent,
      body: new Container(
        width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height,
        margin: MediaQuery.of(context).size.width < 800.0 ? new EdgeInsets.symmetric(horizontal: 20.0) : new EdgeInsets.all(0.0),
        child: new SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: new Column(
            children: [
              new Container(height: MediaQuery.of(context).size.height*0.1),
              new AnimatedContainer(
                width: 600.0,
                curve: Curves.easeOutExpo,
                duration: new Duration(seconds: 1),
                padding: new EdgeInsets.only(),
                decoration: new BoxDecoration(
                  color: Theme.of(context).cardColor,
                  borderRadius: new BorderRadius.all(Radius.circular(5.0)),
                ),
                child: new Stack(
                  alignment: Alignment.center,
                  children: [
                    creating ? new Padding(
                      padding: new EdgeInsets.only(top: 70.0, bottom: 70.0),
                      child: new Container(
                        width: 200.0, height: 200.0,
                        child: new CircularProgressIndicator(
                          valueColor: new AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
                        ),
                      ),
                    ):
                    new Column(
                      children: [
                        showNameWarning ?  new Padding(
                          padding: new EdgeInsets.only(top: 20.0, left: 20.0),
                          child: new Row(
                            children: [
                              new Text("An app name is required ",
                                style: new TextStyle(color: Colors.redAccent, fontSize: 14.0),
                              ),
                            ],
                          )
                        ): new Container(),
                        new Padding(
                          padding: new EdgeInsets.only(top: showNameWarning ? 0.0 :10.0, left: 20.0),
                          child: new TextField(
                            onChanged: (String change){
                              name = change;
                            },
                            onSubmitted: (change){
                              name = change;
                            },
                            autocorrect: true, 
                            autofocus: true,
                            keyboardAppearance: Theme.of(context).brightness, 
                            keyboardType: TextInputType.text, 
                            cursorColor: Theme.of(context).primaryColor, 
                            textCapitalization: TextCapitalization.sentences,
                            maxLines: null, 
                            cursorRadius: Radius.circular(3.0), 
                            cursorWidth: 3.0,
                            style: Theme.of(context).textTheme.headline2,
                            decoration: new InputDecoration(
                              hintStyle: Theme.of(context).textTheme.headline2,
                              hintText: "Enter App Name",
                              border: new UnderlineInputBorder(
                                borderSide: BorderSide.none,
                              )
                            )
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(top: 0.0, left: 20.0),
                          child: new Row(
                            children: [
                              new Text("App Version",
                                style: new TextStyle(color: Theme.of(context).primaryColor, fontSize: 14.0),
                              ),
                            ],
                          )
                        ),
                        showVersionWarning ?  new Padding(
                          padding: new EdgeInsets.only(top: 0.0, left: 20.0, right: 0.0),
                          child: new Row(
                            children: [
                              new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  new Text("An app version is required. This must be in the format of 'int.int.int'\nfor example 1.2.34 ",
                                    style: new TextStyle(color: Colors.redAccent, fontSize: 14.0),
                                  ),
                                ],
                              )
                            ],
                          )
                        ): new Container(),
                        new Padding(
                          padding: new EdgeInsets.only(top: 0.0, left: 20.0),
                          child: new TextField(
                            onChanged: (String change){
                              version = change;
                            },
                            onSubmitted: (change){
                              version = change;
                            },
                            autocorrect: true, 
                            autofocus: true,
                            keyboardAppearance: Theme.of(context).brightness, 
                            keyboardType: TextInputType.number, 
                            cursorColor: Theme.of(context).primaryColor, 
                            textCapitalization: TextCapitalization.sentences,
                            maxLines: null, 
                            cursorRadius: Radius.circular(3.0), 
                            cursorWidth: 3.0,
                            style: Theme.of(context).textTheme.headline4,
                            decoration: new InputDecoration(
                              hintStyle: Theme.of(context).textTheme.headline4,
                              hintText: "1.0.0",
                              border: new UnderlineInputBorder(
                                borderSide: BorderSide.none,
                              )
                            )
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(top: 10.0),
                          child: new IconPicker(
                            showeEditButton: true,
                            iconColor: myColors[color],
                            selected: myIcons[icon],
                            onChange: (int i){
                              setState(() {
                                icon = i;
                              });
                            },
                          )
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(top: 30.0),
                          child: new ColorPicker(
                            onSelected: (int c){
                              setState(() {
                                color = c;
                              });
                            },
                            colors: myColors, 
                            columns: null
                          )
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(right: 0.0),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              new RowButton(title: "ADD APP", onPressed: (){
                                addApp();
                              }),
                            ],
                          )
                        )
                      ],
                    ),
                    new Positioned(
                      top: 0.0, right: 0.0,
                      child: new CloseButton(),
                    )
                  ],
                )
              )
            ],
          ),
        ),
      ),
    );
  }
}


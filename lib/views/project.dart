
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:toast/models/friend/data.dart';
import 'package:toast/models/member/data.dart';
import 'package:toast/models/member/document.dart';
import 'package:toast/models/project/data.dart';
import 'package:toast/models/project/document.dart';
import 'package:toast/models/request/data.dart';
import 'package:toast/repositories/board.dart';
import 'package:toast/repositories/projects.dart';
import 'package:toast/repositories/requests.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/appbar/appbar.dart';
import 'package:toast/toast/board/board.dart';
import 'package:toast/toast/dialog/dialog.dart';
import 'package:toast/toast/drawer/generate.dart';
import 'package:toast/toast/drawer/layout.dart';
import 'package:toast/toast/friends/friendItem.dart';
import 'package:toast/toast/members/board.dart';
import 'package:toast/toast/picker/list.dart';
import 'package:toast/views/addApp.dart';
import 'package:toast/views/addVersion.dart';
import 'package:toast/views/manage.dart';
import 'package:toast/widgets/appbar/folding.dart';
import 'package:toast/widgets/buttons/chipButton.dart';
import 'package:toast/widgets/buttons/rowButton.dart';
import 'package:toast/widgets/cards/card.dart';
import 'package:toast/widgets/searchbar/simple.dart';
import 'package:toast/widgets/selector/splashSelector.dart';

class UniqueProjectView extends StatefulWidget {
  final ProjectRepository projectRepository;

  UniqueProjectView({
    @required this.projectRepository,
  });

  @override
  _UniqueProjectViewState createState() => _UniqueProjectViewState();
}

class _UniqueProjectViewState extends State<UniqueProjectView> {

  GlobalKey<ScaffoldState> scaffoldKey;
  PageController pageController;

  added(){
    setState(() {
      
    });
  }

  onInvite(){
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Theme.of(context).backgroundColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      builder: (_) {
        return DraggableScrollableSheet(
          initialChildSize: 0.8,
          expand: false,
          builder: (_, controller) {
            return new MyFriendList(
              projectRepository: widget.projectRepository,
              members: widget.projectRepository.members,
              friends: UserRepository.userDocument.profile.friends,
              scrollController: controller,
              onComplete: (){
                added();
              },
            );
          }
        );
      }
    );
  }

  update(){
    setState(() {
      
    });
  }

  addBoard(String title){
    widget.projectRepository.createNewBoard(title);
    setState(() {
      
    });
  }

  @override
  void initState() {
    scaffoldKey = new GlobalKey<ScaffoldState>();
    pageController = new PageController();
    widget.projectRepository.addListener(update);

    super.initState();
  }

  @override
  void dispose() {
    pageController.dispose();
    widget.projectRepository?.removeListener(update);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      drawer: new DrawerLayout(
        pages: DrawerPages.generatePages(context, widget?.projectRepository?.project?.data?.name ?? "")
      ),
      appBar: new PreferredSize(
        child: new ToastAppBar(
          userColor: myColors[widget?.projectRepository?.project?.data?.colorIndex ?? 0],
          primary: new IconButton(
            icon: new SvgPicture.asset('assets/menu.svg', height: 13.0,), onPressed: (){
              scaffoldKey.currentState.openDrawer();
            }
          ),
          title: widget?.projectRepository?.project?.data?.name ?? "",
          leading: new SplashSelector(
            color: myColors[widget?.projectRepository?.project?.data?.colorIndex ?? 0],
            items: ["Tasks", "Issues", "Team"],
            pageController: pageController,
          ),
          action: new RowButton(
            title: "NEW VERSION",
            color: myColors[widget?.projectRepository?.project?.data?.colorIndex ?? 0],
            onPressed: (){
              ToastDialog.show(
                context: context,
                child: new AddVersion(
                  onConfirm: (String v){
                    addBoard(v);
                  },
                )
              );
            },
          )
        ), 
        preferredSize: Size(double.infinity, 100.0)
      ),
      body: new PageView(
        controller: pageController,
        children: [
          new BoardList(
            projectRepository: widget.projectRepository,
            boards: widget.projectRepository.boards
          ),
          new BoardList(
            showCurrent: false,
            projectRepository: widget.projectRepository,
            boards: widget.projectRepository.issues,
          ),
          new MembersBoard(
            users: widget.projectRepository.members,
            onInvite: (){
              onInvite();
            },
          )
        ]
      )
    );
  }
}

class MyFriendList extends StatefulWidget {
  final ScrollController scrollController;
  final List<FriendData> friends;
  final List<MemberDocument> members;
  final ProjectRepository projectRepository;
  final Function onComplete;

  MyFriendList({
    @required this.scrollController, @required this.friends, @required this.members, @required this.projectRepository, @required this.onComplete
  });

  @override
  _MyFriendListState createState() => _MyFriendListState();
}

class _MyFriendListState extends State<MyFriendList> {

  List<MemberDocument> members = []; 

  bool isMemberOfProject(String friendId){
    bool contains = false;
    print(friendId);
    for (var i = 0; i < members.length; i++) {
      if (members[i].memberData.uid == friendId){
        contains = true;
      }
    }
    return contains;
  }

  addFriendToProject(FriendData friendData){
    bool approved = FirebaseAuth.instance.currentUser.uid == widget.projectRepository.project.data.owner;
    members.add(
      new MemberDocument(
        id: friendData.uid, 
        memberData: new MemberData(
          name: friendData.name, 
          uid: friendData.uid, 
          approved: approved
        )
      )
    );
    widget.projectRepository.addMemberToProject(members.last);
    
    UserRepository.requests.sendRequest(
      new RequestData(
        name: widget.projectRepository.project.data.name, 
        type: "projectRequest", 
        receiver: friendData.uid, 
        sender: FirebaseAuth.instance.currentUser.uid, 
        identifier: widget.projectRepository.project.id
      )
    );
    setState(() {
      
    });
    widget.onComplete();
  }

  @override
  void initState() {
    members = widget.members;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new CustomScrollView(
      controller: widget?.scrollController,
      slivers: [
        new SliverPersistentHeader(
          pinned: false, floating: true,
          delegate: new FoldingAppBarDelegate(
            height: 100.0,
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                new Container(
                  alignment: Alignment.centerLeft,
                  padding: new EdgeInsets.only(left: 20.0),
                  child: new Text("My Friends",
                    style: Theme.of(context).textTheme.headline3,
                  ),
                ),
                new RowButton(
                  onPressed: (){
                    Navigator.pushAndRemoveUntil(context,
                      MaterialPageRoute(builder: (context) => new ManagePage(openFriendList: true,),), 
                      (Route<dynamic> route) => false
                    );
                  },
                  title: "Find Friends",
                )
              ],
            )
          )
        ),
        new SliverList(
          delegate: new SliverChildListDelegate.fixed(
            List.generate(widget.friends.length, (i){
              return new Padding(
                padding: new EdgeInsets.only(left: 20.0, right: 20.0,),
                child: new FriendInvite(
                  added: isMemberOfProject(widget.friends[i].uid),
                  friend: widget.friends[i],
                  onAddPressed: (){
                    addFriendToProject(widget.friends[i]);
                  },
                )
              );
            })
          ),
        )
      ],
    );
  }
}

class FriendInvite extends StatefulWidget {
  final FriendData friend;
  final bool added; 
  final Function onAddPressed;

  FriendInvite({
    @required this.friend, this.added = false, @required this.onAddPressed
  });

  @override
  _FriendInviteState createState() => _FriendInviteState();
}

class _FriendInviteState extends State<FriendInvite> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          new Row(
            children: [
              new CircleAvatar(
                radius: 18.0,
                backgroundColor: Color(0XFF2E3340),
                child: new Icon(Icons.person, color: Colors.white, size: 19.0,),
              ),
              new Container(width: 10.0,),
              new Text(widget?.friend?.name ?? "Unknown"),
            ],
          ),
          new Container(
            child: widget.added ? 
              new IconButton(icon: new Icon(Icons.check, color: Colors.greenAccent,), onPressed: (){},)
              :new ChipButton(
                padding: new EdgeInsets.only(left: 5.0, right: 5.0, top: 2.0, bottom: 2.0),
                onTap: (){
                  widget.onAddPressed();
                }, 
                text: "Add Friend To Project"
              )
          )
        ],
      ),
    );
  }
}

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:toast/models/board/data.dart';
import 'package:toast/models/project/data.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/picker/colorPicker.dart';
import 'package:toast/toast/picker/iconPicker.dart';
import 'package:toast/views/dashboard.dart';
import 'package:toast/widgets/buttons/rowButton.dart';

class AddVersion extends StatefulWidget {
  final Function(String) onConfirm;
  AddVersion({@required this.onConfirm});

  @override
  _AddVersionState createState() => _AddVersionState();
}

class _AddVersionState extends State<AddVersion> {

  String version;
  Color color = Colors.white;
  bool showVersionWarning = false;

  bool verifyVersion(String v){
    try {
      RegExp versionExp = RegExp(r'(\d+\.)(\d+\.)(\d+)'); 
      String test = versionExp.stringMatch(v);
      if (test.length == v.length){
        return true;
      }
      return false;
    } catch(e){
      return false;
    }
  }

  addVersion(){
    try {
      if (version == null || version == "" || !verifyVersion(version)){
        setState(() {
          showVersionWarning = true;
        });
        return;
      }
      widget.onConfirm(version);
    } catch(e){
      print(e.toString());
    }
    Navigator.of(context).pop();
  }
  
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.transparent,
      body: new Container(
        width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height,
        margin: MediaQuery.of(context).size.width < 800.0 ? new EdgeInsets.symmetric(horizontal: 20.0) : new EdgeInsets.all(0.0),
        child: new SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: new Column(
            children: [
              new Container(height: MediaQuery.of(context).size.height*0.1),
              new Container(
                width: 600.0,
                padding: new EdgeInsets.only(),
                decoration: new BoxDecoration(
                  color: Theme.of(context).cardColor,
                  borderRadius: new BorderRadius.all(Radius.circular(5.0)),
                ),
                child: new Stack(
                  children: [
                    new Column(
                      children: [
                        showVersionWarning ?  new Padding(
                          padding: new EdgeInsets.only(top: 20.0, left: 20.0, right: 0.0),
                          child: new Row(
                            children: [
                              new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  new Text("An app version is required. This must be in the format of 'int.int.int'\nfor example 1.2.3 ",
                                    style: new TextStyle(color: Colors.redAccent, fontSize: 14.0),
                                  ),
                                ],
                              )
                            ],
                          )
                        ): new Container(),
                        new Padding(
                          padding: new EdgeInsets.only(top: 10.0, left: 20.0, bottom: 100.0),
                          child: new TextField(
                            onChanged: (String change){
                              version = change;
                            },
                            onSubmitted: (change){
                              version = change;
                            },
                            autocorrect: true, 
                            autofocus: true,
                            keyboardAppearance: Theme.of(context).brightness, 
                            keyboardType: TextInputType.text, 
                            cursorColor: Theme.of(context).primaryColor, 
                            textCapitalization: TextCapitalization.sentences,
                            maxLines: null, 
                            cursorRadius: Radius.circular(3.0), 
                            cursorWidth: 3.0,
                            style: Theme.of(context).textTheme.headline2,
                            decoration: new InputDecoration(
                              hintStyle: Theme.of(context).textTheme.headline2,
                              hintText: "Enter Version Number",
                              border: new UnderlineInputBorder(
                                borderSide: BorderSide.none,
                              )
                            )
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(right: 0.0),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              new RowButton(title: "ADD VERSION", onPressed: (){
                                addVersion();
                              }),
                            ],
                          )
                        )
                      ],
                    ),
                    new Positioned(
                      top: 0.0, right: 0.0,
                      child: new CloseButton(),
                    )
                  ],
                )
              )
            ],
          ),
        ),
      ),
    );
  }
}


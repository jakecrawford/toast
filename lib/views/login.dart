
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/appbar/appbar.dart';
import 'package:toast/views/dashboard.dart';
import 'package:toast/widgets/buttons/rowButton.dart';
import 'package:toast/widgets/login/view.dart';
import 'package:toast/widgets/selector/splashSelector.dart';

class ToastLogin extends StatefulWidget {
  @override
  _ToastLoginState createState() => _ToastLoginState();
}

class _ToastLoginState extends State<ToastLogin> {

  @override
  void initState() {
  
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return LoginPage(
      onLogin: () async {
        print("LOGIN SUCCESS");
        await UserRepository.init();
        Navigator.pushAndRemoveUntil(context,
          MaterialPageRoute(builder: (context) => new DashboardView(),), 
          (Route<dynamic> route) => false
        );
      },
      background: new Stack(
        children: [
          new FakeLayout(),
          new Container(
            width: double.infinity, height: double.infinity,
            color: Colors.black.withOpacity(0.5),
          ),
        ],
      ),
    );
  }
}

class FakeLayout extends StatefulWidget {
  @override
  _FakeLayoutState createState() => _FakeLayoutState();
}

class _FakeLayoutState extends State<FakeLayout> {

  PageController pageController;

  @override
  void initState() {
    pageController = new PageController();

    super.initState();
  }

  @override
  void dispose() {
    pageController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new PreferredSize(
        child: new ToastAppBar(
          primary: new IconButton(
            icon: new SvgPicture.asset('assets/menu.svg', height: 13.0,), onPressed: (){
              
            }
          ),
          title: "Dashboard",
          leading: new SplashSelector(
            pageController: pageController,
            items: ["Apps", "Issues"],
          ),
          action: new RowButton(
            title: "NEW APP",
            onPressed: (){
              
            },
          )
        ), 
        preferredSize: Size(double.infinity, 100.0)
      ),
      body: new Container()
    );
  }
}



import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/views/dashboard.dart';
import 'package:toast/views/login.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  bool hasUser = false;

  load() async {
    print("LOADING");
    User user = FirebaseAuth.instance.currentUser;
    hasUser = !(user == null);
    if (hasUser){
      print("HAS USER" + user.toString());
      await UserRepository.init();
    } else {
      print("NO USER");
    }
    setState(() {
      
    });
  }

  @override
  void initState() {
    print("IN SPLASHSCREEN");
    load();
    print("HAS USER = " + hasUser.toString());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Container(
        child: hasUser ? new DashboardView() :new ToastLogin()
      ),
    );
  }
}
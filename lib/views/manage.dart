

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:toast/models/friend/data.dart';
import 'package:toast/models/user/data.dart';
import 'package:toast/repositories/projects.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/appbar/appbar.dart';
import 'package:toast/toast/board/board.dart';
import 'package:toast/toast/dialog/dialog.dart';
import 'package:toast/toast/drawer/generate.dart';
import 'package:toast/toast/drawer/layout.dart';
import 'package:toast/toast/friends/addFriends.dart';
import 'package:toast/toast/friends/friends.dart';
import 'package:toast/toast/projects/board.dart';
import 'package:toast/views/addApp.dart';
import 'package:toast/views/addVersion.dart';
import 'package:toast/widgets/buttons/rowButton.dart';
import 'package:toast/widgets/cards/card.dart';
import 'package:toast/widgets/selector/splashSelector.dart';

class ManagePage extends StatefulWidget {
  final bool openFriendList;
  ManagePage({
    this.openFriendList = false,
  });
  @override
  _ManagePageState createState() => _ManagePageState();
}

class _ManagePageState extends State<ManagePage> {

  GlobalKey<ScaffoldState> scaffoldKey;
  PageController pageController;

  removeFriend(FriendData data){
    UserData user = UserRepository.userDocument.profile;
    List<FriendData> myFriends = user.friends;
    for (var i = 0; i < myFriends.length; i++) {
      if (myFriends[i].uid == data.uid){
        myFriends.removeAt(i);
      }
    }
    user.friends = myFriends;
    UserRepository.updateUser(user);
    setState(() {
      
    });
  }

  updatePage(){
    if (mounted){
      setState(() {
        
      });
    }
  }

  showUsers(){
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Theme.of(context).backgroundColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      builder: (_) {
        return DraggableScrollableSheet(
          initialChildSize: 0.8,
          expand: false,
          builder: (_, controller) {
            return new AddFriends(
              scrollController: controller,
              onUpdate: (){
                updatePage();
              },
            );
          }
        );
      }
    );
  }

  checkFlags(){
    if (widget.openFriendList && mounted){
      showUsers();
    }
  }

  @override
  void initState() {
    scaffoldKey = new GlobalKey<ScaffoldState>();
    pageController = new PageController();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      checkFlags();
    });

    super.initState();
  }

  @override
  void dispose() {
    pageController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      drawer: new DrawerLayout(
        pages: DrawerPages.generatePages(context, "manage")
      ),
      appBar: new PreferredSize(
        child: new ToastAppBar(
          primary: new IconButton(
            icon: new SvgPicture.asset('assets/menu.svg', height: 13.0,), onPressed: (){
              scaffoldKey.currentState.openDrawer();
            }
          ),
          title: "Manage",
          leading: new SplashSelector(
            items: ["Friends", "Apps"],
            pageController: pageController,
          ),
          action: new PageSwitchList(
            pageController: pageController,
            children: [
              new RowButton(
                title: "ADD FRIENDS",
                onPressed: (){
                  showUsers();
                },
              ),
              new RowButton(
                title: "ADD APPS",
                onPressed: (){
                  ToastDialog.show(
                    context: context,
                    child: new AddApp(
                      
                    )
                  );
                },
              )
            ],
          )
        ), 
        preferredSize: Size(double.infinity, 100.0)
      ),
      body: new PageView(
        controller: pageController,
        children: [
          new SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            child: new Padding(
              padding: new EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
              child: new FloatingCard(
                child: new FriendBoard(
                  users: List.generate(UserRepository.userDocument?.profile?.friends?.length ?? 0, (index){
                    return UserRepository.userDocument.profile.friends[index];
                  })
                )
              )
            ),
          ),
          new SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            child: new Padding(
              padding: new EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
              child: new FloatingCard(
                child: new ProjectBoard(
                  projects: List.generate(UserRepository.projects.length ?? 0, (index){
                    return UserRepository.projects[index];
                  })
                )
              )
            ),
          )
        ]
      )
    );
  }
}

class PageSwitchList extends StatefulWidget {
  final List<Widget> children;
  final PageController pageController;
  PageSwitchList({
    this.children = const [], @required this.pageController
  });
  @override
  _PageSwitchListState createState() => _PageSwitchListState();
}

class _PageSwitchListState extends State<PageSwitchList> {

  int selected = 0;

  update(){
    try {
      selected = (widget.pageController.page).round();
      setState(() {
        
      });
    } catch(e){

    }
  }

  @override
  void initState() {
    widget.pageController.addListener(update);

    super.initState();
  }

  @override
  void dispose() {
    widget.pageController?.removeListener(update);

    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: new Duration(seconds: 1),
      child: widget.children[selected],
    );
  }
}
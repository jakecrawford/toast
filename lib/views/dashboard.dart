import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:toast/models/project/document.dart';
import 'package:toast/repositories/board.dart';
import 'package:toast/repositories/projects.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/appbar/appbar.dart';
import 'package:toast/toast/board/board.dart';
import 'package:toast/toast/dialog/dialog.dart';
import 'package:toast/toast/drawer/generate.dart';
import 'package:toast/toast/drawer/layout.dart';
import 'package:toast/views/addApp.dart';
import 'package:toast/widgets/buttons/rowButton.dart';
import 'package:toast/widgets/selector/splashSelector.dart';

class DashboardView extends StatefulWidget {

  @override
  _DashboardViewState createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {

  GlobalKey<ScaffoldState> scaffoldKey;
  PageController pageController;

  Future loadUser() async {
    await UserRepository.syncProjects();
    if (mounted){
      setState(() {

      });
    }
  }

  List<BoardRepository>buildIssuesList(List<ProjectRepository> projects){
    List<BoardRepository> temp = [];
    try {
      projects.forEach((ProjectRepository projectRepository) {
        temp.add(projectRepository.issues[0]);
      });
    } catch(e){
      print(e.toString());
    }
    return temp;
  }

  List<BoardRepository>buildList(List<ProjectRepository> projects){
    List<BoardRepository> temp = [];
    try {
      projects.forEach((ProjectRepository projectRepository) {
        String target = projectRepository.project.data.primaryBoard;
        for (var i = 0; i < projectRepository.boards.length; i++) {
          if (projectRepository.boards[i].boardDocument.id == target){
            temp.add(projectRepository.boards[i]);
          }
        }
      });
    } catch(e){
      print(e.toString());
    }
    return temp;
  }

  @override
  void initState() {
    scaffoldKey = new GlobalKey<ScaffoldState>();
    pageController = new PageController();
    loadUser();

    super.initState();
  }

  @override
  void dispose() {
    pageController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      drawer: new DrawerLayout(
        pages: DrawerPages.generatePages(context, "home")
      ),
      appBar: new PreferredSize(
        child: new ToastAppBar(
          title: "Home",
          primary: new IconButton(
            icon: new SvgPicture.asset('assets/menu.svg', height: 13.0,), onPressed: (){
              scaffoldKey.currentState.openDrawer();
            }
          ),
          leading: new SplashSelector(
            items: ["Apps", "Issues"],
            pageController: pageController,
          ),
          action: new RowButton(
            title: "NEW APP",
            onPressed: (){
              ToastDialog.show(
                context: context,
                child: new AddApp(
                  
                )
              );
            },
          )
        ), 
        preferredSize: Size(double.infinity, 100.0)
      ),
      body: new PageView(
        controller: pageController,
        children: [
          new BoardList(
            asDashboard: true,
            showAddOnEmpty: true,
            boards: buildList(UserRepository.projects)
          ),
          new BoardList(
            asDashboard: true,
            boards: buildIssuesList(UserRepository.projects),
          ),
        ]
      )
    );
  }
}

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:toast/models/friend/data.dart';
import 'package:toast/models/request/data.dart';
import 'package:toast/models/request/document.dart';
import 'package:toast/models/user/data.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/picker/colorPicker.dart';
import 'package:toast/toast/picker/iconPicker.dart';
import 'package:toast/toast/picker/list.dart';
import 'package:toast/widgets/buttons/chipButton.dart';
import 'package:toast/widgets/buttons/rowButton.dart';

class MyProfile extends StatefulWidget {
  final Function onConfirm;
  MyProfile({@required this.onConfirm});

  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {

  String name;
  Color color = Colors.white;
  int icon = 0;
  TextEditingController textEditingController;
  List<RequestDocument> requestList = [];

  confirmNameChange(){
    if (name == null || name == "") return;
    UserRepository.userDocument.profile.name = name;
    UserRepository.updateUser(UserRepository.userDocument.profile);
  }

  update(){
    setState(() {
      requestList = UserRepository.requests.requests;
    });
  }

  acceptProject(RequestDocument newProject){
    UserRepository.requests.removeRequest(newProject);
    UserRepository.loadProject(UserRepository.projects, newProject.data.identifier);
    UserRepository.userDocument.profile.projects.add(newProject.data.identifier);
    UserRepository.updateUser(UserRepository.userDocument.profile);
    setState(() {
      
    });
  }

  acceptFriend(RequestDocument newFriend){
    UserData user = UserRepository.userDocument.profile;
    user.friends.add(
      new FriendData(name: newFriend.data.name, uid: newFriend.data.identifier)
    );
    UserRepository.requests.removeRequest(newFriend);
    UserRepository.updateUser(user);
    setState(() {
      
    });
  } 

  @override
  void initState() {
    requestList = UserRepository.requests.requests;
    textEditingController = new TextEditingController(text: UserRepository.userDocument.profile.name);
    UserRepository.requests.addListener(update);

    super.initState();
  }
  
  @override
  void dispose() {
    UserRepository.requests.removeListener(update);
    textEditingController.dispose();

    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.transparent,
      body: new Container(
        width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height,
        margin: MediaQuery.of(context).size.width < 800.0 ? new EdgeInsets.symmetric(horizontal: 20.0) : new EdgeInsets.all(0.0),
        child: new SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: new Column(
            children: [
              new Container(height: MediaQuery.of(context).size.height*0.1),
              new AnimatedContainer(
                width: 600.0,
                curve: Curves.easeOutExpo,
                duration: new Duration(seconds: 1),
                padding: new EdgeInsets.only(),
                decoration: new BoxDecoration(
                  color: Theme.of(context).cardColor,
                  borderRadius: new BorderRadius.all(Radius.circular(5.0)),
                ),
                child: new Stack(
                  alignment: Alignment.center,
                  children: [
                    new Column(
                      children: [
                        new Padding(
                          padding: new EdgeInsets.only(top: 10.0, left: 20.0),
                          child: new TextField(
                            controller: textEditingController,
                            onChanged: (String change){
                              name = change;
                            },
                            onSubmitted: (change){
                              name = change;
                            },
                            autocorrect: false, 
                            autofocus: false,
                            keyboardAppearance: Theme.of(context).brightness, 
                            keyboardType: TextInputType.text, 
                            cursorColor: Theme.of(context).primaryColor, 
                            textCapitalization: TextCapitalization.sentences,
                            maxLines: null, 
                            cursorRadius: Radius.circular(3.0), 
                            cursorWidth: 3.0,
                            style: Theme.of(context).textTheme.headline2,
                            decoration: new InputDecoration(
                              hintStyle: Theme.of(context).textTheme.headline2,
                              hintText: UserRepository.userDocument.profile.name ?? "Enter name",
                              border: new UnderlineInputBorder(
                                borderSide: BorderSide.none,
                              )
                            )
                          ),
                        ),
                        new IgnorePointer(
                          child: new Padding(
                            padding: new EdgeInsets.only(top: 10.0),
                            child: new IconPicker(
                              iconColor: color,
                              selected: myIcons[icon],
                              onChange: (int i){
                                setState(() {
                                  icon = i;
                                });
                              },
                            )
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.symmetric(horizontal: 30.0, vertical: 30.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              new Row(
                                children: [
                                  new Text("Notifications", style: Theme.of(context).textTheme.headline4,)
                                ],
                              ),
                              new Container(height: 10.0,),
                              new RequestList(
                                onConfirm: (RequestDocument v){
                                  if (v.data.type == 'friendRequest'){
                                    acceptFriend(v);
                                  } else {
                                    acceptProject(v);
                                  }
                                },
                                requests: requestList,
                              )
                            ],
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(right: 0.0),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              new FlatButton(
                                child: new Text("Done", style: Theme.of(context).textTheme.headline5,),
                                onPressed: (){
                                  confirmNameChange();
                                  widget.onConfirm();
                                },
                              )
                            ],
                          )
                        )
                      ],
                    ),
                    new Positioned(
                      top: 0.0, right: 0.0,
                      child: new CloseButton(
                        onPressed: (){
                          widget.onConfirm();
                        },
                      ),
                    )
                  ],
                )
              )
            ],
          ),
        ),
      ),
    );
  }
}

class RequestList extends StatefulWidget {
  final List<RequestDocument> requests;
  final Function(RequestDocument) onConfirm;

  RequestList({
    @required this.requests, @required this.onConfirm
  });

  @override
  _RequestListState createState() => _RequestListState();
}

class _RequestListState extends State<RequestList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: widget.requests.length > 0 ? new Column(
        children: List.generate(widget.requests.length, (index){
          RequestDocument doc = widget.requests[index];
          if (doc.data.type == "friendRequest"){
            return new RequestItem(
              key: new GlobalKey(),
              title: doc.data.name,
              subtitle: "Friend Request",
              onAccept: (){
                widget.onConfirm(doc);
              },
              onReject: (){

              },
            );
          } else {
            return new RequestItem(
              key: new GlobalKey(),
              title: doc.data.name,
              subtitle: "Project Request",
              onAccept: (){
                widget.onConfirm(doc);
              },
              onReject: (){

              },
            );
          }
        })
      ):
      new Container(
        child: new Text("None"),
      )
    );
  }
}


class RequestItem extends StatefulWidget {
  final String title;
  final String subtitle;
  final Function onAccept;
  final Function onReject;

  RequestItem({
    @required GlobalKey key,  @required this.title, @required this.subtitle, @required this.onAccept, @required this.onReject
  }):super(key: key);

  @override
  _RequestItemState createState() => _RequestItemState();
}

class _RequestItemState extends State<RequestItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          new Row(
            children: [
              new Container(
                color: Colors.transparent,
                child: new Row(
                  children: [
                    new CircleAvatar(
                      radius: 16.0,
                      backgroundColor: Theme.of(context).primaryColor,
                      child: new Icon(Icons.person, color: Colors.white, size: 18.0,),
                    ),
                    new Container(width: 12.0),
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        new Text(widget.title, style: Theme.of(context).textTheme.headline5,),
                        new Transform(
                          transform: new Matrix4.translationValues(-0.0, -2.0, 0.0),
                          child: new Text(widget.subtitle, style: Theme.of(context).textTheme.bodyText1,),
                        )
                      ]
                    )
                  ]
                )
              ),
            ],
          ),
          new ChipButton(
            padding: new EdgeInsets.only(left: 5.0, right: 5.0, top: 2.0, bottom: 2.0),
            onTap: (){
              widget.onAccept();
            }, 
            text: "Accept"
          )
        ],
      ),
    );
  }
}
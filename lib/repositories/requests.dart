

import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:toast/models/request/data.dart';
import 'package:toast/models/request/document.dart';

class RequestsRepository with ChangeNotifier {
  CollectionReference collectionReference = FirebaseFirestore.instance.collection("Requests");
  StreamSubscription subscription;
  List<RequestDocument> requests = [];

  init(){
    syncRequests();
  }

  removeRequest(RequestDocument doc) async {
    try {
      collectionReference.doc(doc.id).delete();
      for (var i = 0; i < requests.length; i++) {
        if (requests[i].id == doc.id){
          requests.removeAt(i);
        }
      }
      notifyListeners();
      FirebaseFirestore.instance.runTransaction((transaction) async {

      },timeout: Duration(seconds: 10));
    } catch(e){
      
    }
  }

  sendRequest(RequestData data){
    collectionReference.doc().set(data.toJson());
    FirebaseFirestore.instance.runTransaction((transaction) async {

    },timeout: Duration(seconds: 10));
  }

  syncRequests() async {
    print("sync requests");
    try {
      QuerySnapshot querySnapshot = await collectionReference.where("receiver", isEqualTo: FirebaseAuth.instance.currentUser.uid).get();
      List<RequestDocument> temp = [];
      for (var i = 0; i < querySnapshot.docs.length; i++) {
        temp.add(
          new RequestDocument.fromQuerySnapshot(querySnapshot.docs[i])
        );
      }
      requests = temp;
      print("requests sucessfully called");
    } catch(e){
      print("sync requests errror");
      print(e.toString());
    }
    startStream();
  }

  startStream(){
    try {
      print("started stream");
      subscription = collectionReference.where("receiver", isEqualTo: FirebaseAuth.instance.currentUser.uid).snapshots().listen((QuerySnapshot snap) {
        List<RequestDocument> temp = [];
        for (var i = 0; i < snap.docs.length; i++) {
        temp.add(
            new RequestDocument.fromQuerySnapshot(snap.docs[i])
          );
        }
        requests = temp;
        notifyListeners();
      });
    } catch(e){
      print("cant stream this");
      print(e.toString());
    }
  }

  cancelStream(){
    subscription.cancel();
  }
}
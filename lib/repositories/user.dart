import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:toast/models/project/data.dart';
import 'package:toast/models/user/data.dart';
import 'package:toast/models/user/document.dart';
import 'package:toast/repositories/projects.dart';
import 'package:toast/repositories/requests.dart';

class UserRepository {
  static UserDocument userDocument;
  static List<ProjectRepository> projects = [];
  static RequestsRepository requests = new RequestsRepository();

  static reset(){
    userDocument = null;
    projects = [];
    requests = new RequestsRepository();
  }

  static Future init() async {
    await fetchUser();
    requests = new RequestsRepository();
    requests.init();
    List<ProjectRepository> temp = [];
    await Future.wait(
      List.generate(userDocument?.profile?.projects?.length ?? 0, (index){
        return loadProject(temp, userDocument.profile.projects[index]);
      })
    );
    projects = temp;
  }

  static createNewUser(String name){
    print(FirebaseAuth.instance.currentUser.uid);
    try {
      FirebaseFirestore.instance.collection("Users").doc(FirebaseAuth.instance.currentUser.uid).set({
        "uid": FirebaseAuth.instance.currentUser.uid,
        "createdOn": DateTime.now(),
        "name": name,
        "projects": [],
        "friends": []
      });
      FirebaseFirestore.instance.runTransaction((transaction) async {
      
      }, timeout: Duration(seconds: 10));
      print("welcome");
    } catch(e){
      print("error creating user");
      print(e.toString());
    }
  }

  static syncProjects(){
    for (var i = 0; i < projects.length; i++) {
      projects[i].syncProject();
    }
  }

  static Future fetchUser() async {
    try {
      User user = FirebaseAuth.instance.currentUser;
      CollectionReference usersRef = FirebaseFirestore.instance.collection("Users");
      QuerySnapshot querySnapshot = await usersRef.where("uid", isEqualTo: FirebaseAuth.instance.currentUser.uid).get();
      userDocument = new UserDocument.fromSnapshot(querySnapshot.docs[0]);
      print("SET USER");
    } catch(e){
      print("error fetching user");
      print(e.toString());
    }
  }

  static createProject(ProjectData data, String version) async{
    try {
      ProjectRepository project = new ProjectRepository();
      await project.createNewProject(data, version);
      print("added here");
      projects.add(project);
      List<String> projectIds = userDocument.profile.projects;
      projectIds.add(project.project.id);
      print("hitq");
      DocumentReference user = FirebaseFirestore.instance.collection("Users").doc(userDocument.id);
      user.update({
        "projects": projectIds
      });
      print("hit 2");
      FirebaseFirestore.instance.runTransaction((transaction) async {
        print("ran transaction");
      },timeout: Duration(seconds: 10));
      fetchUser();
    } catch(e){
      print(e.toString());
    }
  }

  static Future loadProject(List<ProjectRepository> temp, String projectId) async {
    ProjectRepository project = new ProjectRepository();
    await project.init(projectId);
    temp.add(project);
    return;
  }

  static updateUser(UserData v){
    DocumentReference doc = FirebaseFirestore.instance.collection("Users").doc(userDocument.id);
    doc.update(v.toJson());
    FirebaseFirestore.instance.runTransaction((transaction) async {}, timeout: Duration(seconds: 10));
    userDocument.profile = v;
  }
}
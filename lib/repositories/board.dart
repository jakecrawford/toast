
import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:toast/models/board/data.dart';
import 'package:toast/models/board/document.dart';
import 'package:toast/models/task/data.dart';

class BoardRepository extends ChangeNotifier{
  BoardDocument boardDocument;
  CollectionReference boardCollection;
  StreamSubscription subscription;

  init(CollectionReference collection, DocumentSnapshot snap){
    this.boardCollection = collection;
    setBoard(snap);
    print("INIT BOARD REPOSITORY");
    _createListener();
  }

  _createListener(){
    boardCollection.doc(boardDocument.id).get();
    subscription = boardCollection.doc(boardDocument.id).snapshots().listen((DocumentSnapshot snap) {
      boardDocument = new BoardDocument.fromSnapshot(snap);
      notifyListeners();
    });
  }

  _removeListener(){
    subscription.cancel();
  }

  syncBoard() async {
    try {
      DocumentSnapshot snapshot = await boardCollection.doc(boardDocument.id).get();
      boardDocument = new BoardDocument.fromSnapshot(snapshot);

    } catch(e){

    }
    notifyListeners();
  }

  runTransaction(List<TaskData> list) async {
    try {
      DocumentReference doc = boardCollection.doc(boardDocument.id);
      String jsonTags = jsonEncode(list);
      doc.update({
        "tasks": jsonTags
      });
    } catch(e){
    
    }
    FirebaseFirestore.instance.runTransaction((transaction)async{
      
    }, timeout: Duration(seconds: 10));
  }

  addTask(TaskData data){
    try {
      List<TaskData> list = boardDocument?.data?.tasks ?? [];
      list.add(data);
      runTransaction(list);
      boardDocument.data.tasks = list;
    } catch(e){

    }
    notifyListeners();
  }

  updateTask(int index, TaskData data){
    print("updating task");
    try {
      List<TaskData> list = boardDocument?.data?.tasks ?? [];
      list[index] = data;
      runTransaction(list);
      boardDocument.data.tasks = list;
    } catch(e){
      print(e.toString());
    }
  }

  updateDoc(BoardData boardData){
    boardDocument.data = boardData;
    boardCollection.doc(boardDocument.id).update(boardData.toJson());
    syncBoard();
    notifyListeners();
  }

  setBoard(QueryDocumentSnapshot queryDocumentSnapshot){
    boardDocument = new BoardDocument.fromQueryDocumentSnapshot(queryDocumentSnapshot);
  }

  BoardRepository();
}
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:toast/models/board/data.dart';
import 'package:toast/models/board/document.dart';
import 'package:toast/models/member/data.dart';
import 'package:toast/models/member/document.dart';
import 'package:toast/models/project/data.dart';
import 'package:toast/models/project/document.dart';
import 'package:toast/models/task/data.dart';
import 'package:toast/repositories/board.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/board/task.dart';

class ProjectRepository with ChangeNotifier {
  List<BoardRepository> boards = [];
  List<BoardRepository> issues = [];
  List<MemberDocument> members = [];
  ProjectDocument project;

  ProjectRepository();

  Future _syncBoard(BoardRepository repo) async {
    await repo.syncBoard();
  }

  Future syncProject() async {
    DocumentReference projectDoc = FirebaseFirestore.instance.collection("Projects").doc(project.id);
    CollectionReference membersCollection = FirebaseFirestore.instance.collection("Projects").doc(project.id).collection("Members");
    await _fetchProjectDoc(projectDoc);
    await Future.wait([
      Future.wait(List.generate(boards.length, (index) => _syncBoard(boards[index]))),
      Future.wait(List.generate(issues.length, (index) => _syncBoard(issues[index]))),
      _fetchMembers(membersCollection)
    ]);
  }

  addMemberToProject(MemberDocument memberDocument){
    FirebaseFirestore.instance.collection("Projects").doc(project.id).collection("Members").doc(memberDocument.memberData.uid).set({
      "name": memberDocument.memberData.name ?? "Unknown",
      "uid": memberDocument.memberData.uid,
      "approved": memberDocument.memberData.approved
    });
  }

  updateProjectData(ProjectData data){
    DocumentReference projectDoc = FirebaseFirestore.instance.collection("Projects").doc(project.id);
    projectDoc.set(data.toJson());
    FirebaseFirestore.instance.runTransaction((transaction) async {
      
    }, timeout: Duration(seconds: 10));
    notifyListeners();
  }

  createNewBoard(String title) async {
    FirebaseFirestore.instance.collection("Projects")
      .doc(project.id).collection("Boards").doc().set({
      "title": title,
      "subtitle": project.data.name,
      "tasks": [],
      "created": DateTime.now().toIso8601String()
    });

    FirebaseFirestore.instance.runTransaction((transaction) async {
      
    }, timeout: Duration(seconds: 10));

    CollectionReference boardsCollection = FirebaseFirestore.instance.collection("Projects")
      .doc(project.id).collection("Boards");
    await _fetchBoards(boardsCollection);

    notifyListeners();
  }

  createNewProject(ProjectData data, String version) async{
    try {
      DocumentReference projectDoc = FirebaseFirestore.instance.collection("Projects").doc();
      projectDoc.set(data.toJson());

      DocumentReference boardDoc = FirebaseFirestore.instance.collection("Projects")
        .doc(projectDoc.id).collection("Boards").doc();

      DocumentReference issuesDoc = FirebaseFirestore.instance.collection("Projects")
        .doc(projectDoc.id).collection("Issues").doc();

      DocumentReference membersDoc = FirebaseFirestore.instance.collection("Projects")
        .doc(projectDoc.id).collection("Members").doc(FirebaseAuth.instance.currentUser.uid);

      membersDoc.set({
        "name": UserRepository.userDocument.profile.name ?? "Unknown",
        "uid": FirebaseAuth.instance.currentUser.uid,
        "approved": true,
      });
        
      issuesDoc.set({
        "title": "Issues",
        "subtitle": data.name,
        "tasks": []
      });
      
      boardDoc.set({
        "title": version,
        "subtitle": data.name,
        "tasks": [],
        "created": DateTime.now().toIso8601String()
      });
      
      ProjectData updated = data;
      updated.primaryBoard = boardDoc.id;
      projectDoc.set(updated.toJson());
      project = new ProjectDocument(data: data, id: projectDoc.id);

      await FirebaseFirestore.instance.runTransaction((transaction) async {
      
      }, timeout: Duration(seconds: 10));

      await init(projectDoc.id);

    } catch(e){
      print(e.toString());
    }
  }

  Future _fetchProjectDoc(DocumentReference doc) async {
    try {
      DocumentSnapshot snapshot = await doc.get();
      project = new ProjectDocument.fromSnapshot(snapshot);
      print("fetch proejct doc");
      notifyListeners();
    } catch(e){
      print(e.toString());
    }
  }

  Future _fetchBoards(CollectionReference collection) async {
    try {
      QuerySnapshot querySnapshot = await collection.orderBy('created', descending: true).get();
      List<BoardRepository> repositories = [];
      querySnapshot.docs.forEach((QueryDocumentSnapshot element) {
        BoardRepository board = new BoardRepository();
        board.init(collection, element);
        repositories.add(board);
      });
      boards = repositories;
      print("fetched boards");
    } catch(e){
      print("board error");
      print(e.toString());
    }
  }

  Future _fetchMembers(CollectionReference collection) async {
    try {
      QuerySnapshot querySnapshot = await collection.get();
      List<MemberDocument> temp = [];
      querySnapshot.docs.forEach((QueryDocumentSnapshot element) {
        temp.add(new MemberDocument.fromQuerySnapshot(element));
      });
      members = temp;
      print("members set");
    } catch(e){
      print("member error");
    }
  }

  Future _fetchIssues(CollectionReference collection) async {
    try {
      List<BoardRepository> repositories = [];
      QuerySnapshot querySnapshot = await collection.get();
      BoardRepository board = new BoardRepository();
      board.init(collection, querySnapshot.docs[0]);
      repositories.add(board);
      issues = repositories;
      print("issues set");
    } catch(e){
      print("issues error");
      print(e.toString());
    }
  }

  Future init(String documentId) async {
    print("init");
    DocumentReference projectDoc = FirebaseFirestore.instance.collection("Projects").doc(documentId);
    CollectionReference boardsCollection = FirebaseFirestore.instance.collection("Projects")
      .doc(documentId).collection("Boards");
    CollectionReference membersCollection = FirebaseFirestore.instance.collection("Projects")
      .doc(documentId).collection("Members");
    CollectionReference issuesCollection = FirebaseFirestore.instance.collection("Projects")
      .doc(documentId).collection("Issues");

    await Future.wait([
      _fetchProjectDoc(projectDoc),
      _fetchBoards(boardsCollection),
      _fetchMembers(membersCollection),
      _fetchIssues(issuesCollection),
    ]);
    print("init end");
  }
}
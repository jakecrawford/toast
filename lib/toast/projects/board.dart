
import 'package:flutter/material.dart';
import 'package:toast/models/friend/data.dart';
import 'package:toast/repositories/projects.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/friends/friendItem.dart';
import 'package:toast/toast/projects/projectItem.dart';

class ProjectBoard extends StatefulWidget {
  final List<ProjectRepository> projects;
  final String emptyString;
  ProjectBoard({@required this.projects, this.emptyString = "You have no friends"});

  @override
  _ProjectBoardState createState() => _ProjectBoardState();
}

class _ProjectBoardState extends State<ProjectBoard> {

  @override
  void initState() {

    super.initState();
  }

  @override
  void dispose() {

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: new Duration(milliseconds: 500),
      curve: Curves.ease,
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          new Padding(
            padding: new EdgeInsets.only(top: 20.0, left: 20.0),
            child: new Text("All",
              style: new TextStyle(
                color: Theme.of(context).primaryColor, fontFamily: "Nunito", fontWeight: FontWeight.w700, fontSize: 14.0
              ),
            ),
          ),
          new Padding(
            padding: new EdgeInsets.only(top: 5.0, left: 20.0, bottom: 10.0),
            child: new Text("My Apps",
              style: new TextStyle(
                color: Colors.white, fontFamily: "Nunito", fontWeight: FontWeight.w700, fontSize: 32.0
              ),
            ),
          ),
          widget.projects.length == 0 ? new Container(
            child: new Padding(
              padding: new EdgeInsets.only(left: 20.0, bottom: 10.0),
              child: new Text("You have no projects"),
            )
          ):
          new Column(
            children: List.generate(widget.projects.length, (index){
              return new Padding(
                padding: new EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10.0),
                child: new ProjectItem(
                  onPressed: (v){

                  }, 
                  projectRepository: widget.projects[index]
                )
              );
            }),
          ),
          new Container(height: 10.0),
        ]
      ),
    );
  }
}




import 'package:flutter/material.dart';
import 'package:toast/models/friend/data.dart';
import 'package:toast/repositories/projects.dart';
import 'package:toast/widgets/buttons/chipButton.dart';

class ProjectItem extends StatefulWidget {
  final ProjectRepository projectRepository;
  final Function(ProjectRepository) onPressed; 

  ProjectItem({
    @required this.onPressed, @required this.projectRepository,
  });
  
  @override
  _ProjectItemState createState() => _ProjectItemState();
}

class _ProjectItemState extends State<ProjectItem> {
  
  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          new Row(
            children: [
              new CircleAvatar(
                radius: 18.0,
                backgroundColor: Color(0XFF2E3340),
                child: new Icon(Icons.person, color: Colors.white, size: 19.0,),
              ),
              new Container(width: 10.0,),
              new Text(widget?.projectRepository?.project?.data?.name ?? "Unknown"),
            ],
          ),
        ],
      ),
    );
  }
}
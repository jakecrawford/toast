
import 'package:flutter/material.dart';
import 'package:toast/models/board/document.dart';
import 'package:toast/models/task/data.dart';
import 'package:toast/repositories/board.dart';
import 'package:toast/repositories/projects.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/board/input.dart';
import 'package:toast/toast/board/labels.dart';
import 'package:toast/toast/board/task.dart';
import 'package:toast/toast/picker/list.dart';
import 'package:toast/widgets/buttons/chipButton.dart';
import 'package:toast/widgets/buttons/radioRowButton.dart';

class BoardLayout extends StatefulWidget {
  final BoardRepository boardRepository;
  final ProjectRepository projectRepository;
  final bool showCurrent;
  final bool asDashboard;

  BoardLayout({
    @required this.boardRepository, this.projectRepository, this.showCurrent = true, this.asDashboard = false,
  });

  @override
  _BoardLayoutState createState() => _BoardLayoutState();
}

class _BoardLayoutState extends State<BoardLayout> {

  GlobalKey<BoardInputState> inputKey;
  bool editing = false;

  changeCurrent(){
    widget.projectRepository.project.data.primaryBoard = widget.boardRepository.boardDocument.id;
    widget.projectRepository.updateProjectData(widget.projectRepository.project.data);
    setState(() {
      
    });
  }

  updateBoardItem(int index, TaskData value) async {
    await widget.boardRepository.updateTask(index, value);
    setState(() {
      
    });
  }

  saveItemToBoard(BoardInputGroup input){
    try {
      TaskData data = new TaskData(
        priority: input.priority["selected"].id, 
        progress: input.progress["selected"].id, 
        user: UserRepository.userDocument.profile.name ?? "Unknown1", 
        task: input.text
      );
      widget.boardRepository.addTask(data);
      inputKey.currentState.reset();
    } catch(e){
      print(e.toString());
    }
  }

  deleteItemFromBoard(int index){
    print("removing" + index.toString());
    List<TaskData> data = widget.boardRepository.boardDocument.data.tasks;
    data.removeAt(index);
    print(data.length.toString() + "Lennnnnn");
    widget.boardRepository.boardDocument.data.tasks = data;
    widget.boardRepository.updateDoc(widget.boardRepository.boardDocument.data);
    setState(() {
      
    });
  }

  updateState(){
    setState(() {
      print("recieved notification to update board");
    });
  }

  @override
  void didUpdateWidget(covariant BoardLayout oldWidget) {
    try {
      widget.boardRepository.removeListener(updateState);
      widget.boardRepository.addListener(updateState);
    } catch(e){
      print(e.toString());
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    widget.boardRepository.addListener(updateState);
    inputKey = new GlobalKey<BoardInputState>();

    super.initState();
  }

  @override
  void dispose() {
    widget.boardRepository?.removeListener(updateState);
    
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: new Duration(milliseconds: 500),
      curve: Curves.ease,
      child: new Stack(
        children: [
          new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              new Padding(
                padding: new EdgeInsets.only(top: 20.0, left: 20.0),
                child: new Text(widget.boardRepository.boardDocument.data.subtitle ?? "",
                  style: new TextStyle(
                    color: widget.asDashboard ? Color(0XFF6C758E) : myColors[widget?.projectRepository?.project?.data?.colorIndex ?? 0] ?? Theme.of(context).primaryColor, fontFamily: "Nunito", fontWeight: FontWeight.w700, fontSize: 14.0,
                  ),
                ),
              ),
              new Padding(
                padding: new EdgeInsets.only(top: 5.0, left: 20.0, bottom: 10.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    new Text(widget.boardRepository.boardDocument.data.title ?? "",
                      style: new TextStyle(
                        color: Colors.white, fontFamily: "Nunito", fontWeight: FontWeight.w700, fontSize: 32.0
                      ),
                    ),
                    MediaQuery.of(context).size.width < 800.0 ? new Container(): 
                    new Row(
                      children: [
                        new Padding(
                          padding: new EdgeInsets.only(right: 30.0),
                          child: new Container(
                            width: 80.0,
                            alignment: Alignment.center,
                            child: new Text("STATUS", style: new TextStyle(color: Color(0XFF636879), fontSize: 10.0)),
                          )
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(right: 30.0),
                          child: new Container(
                            width: 80.0,
                            alignment: Alignment.center,
                            child: new Text("PRIORITY", style: new TextStyle(color: Color(0XFF636879), fontSize: 10.0)),
                          )
                        )
                      ],
                    )
                  ],
                )
              ),
              new Column(
                children: List.generate(widget.boardRepository.boardDocument.data.tasks.length, (index){
                  return new Padding(
                    padding: new EdgeInsets.only(bottom: MediaQuery.of(context).size.width < 800.0 ? 20.0 : 0.0),
                    child: new TaskItem(
                      key: new GlobalKey(),
                      isEditing: editing,
                      onDelete: (){
                        deleteItemFromBoard(index);
                      },
                      onChange: (TaskData value){
                        updateBoardItem(index, value);
                      },
                      taskData: widget.boardRepository.boardDocument.data.tasks[index]
                    ),
                  );
                }),
              ),
              new Container(height: MediaQuery.of(context).size.width < 800.0 ? 20.0 : 10.0),
              new IgnorePointer(
                ignoring: editing,
                child: new AnimatedOpacity(
                  opacity: editing ? 0.2 : 1.0, 
                  curve: Curves.easeOutExpo,
                  duration: new Duration(milliseconds: 500),
                  child: new BoardInput(
                    key: inputKey,
                    onConfirm: (BoardInputGroup input){
                      saveItemToBoard(input);
                    }, 
                    input: new BoardInputGroup(
                      text: "", 
                      priority: priorityLabels, 
                      progress: progressLabels
                    )
                  ) ,
                ),
              )
            ]
          ),
          widget.projectRepository == null ? new Container()
          : new Positioned(
            top: 0.0, right: 5.0,
            child: new Row(
              children: [
                widget.showCurrent ? new IgnorePointer(
                  ignoring: !editing,
                  child: new Row(
                    children: [
                      new AnimatedOpacity(
                        opacity: editing ? 1.0 : 0.0, 
                        duration: new Duration(milliseconds: 500),
                        curve: Curves.easeOutExpo,
                        child: new RadioRowButton(
                        padding: new EdgeInsets.symmetric(vertical: 10.0),
                          title: "Current",
                          onChange: (v){
                            changeCurrent();
                          },
                          selected: widget.projectRepository.project.data.primaryBoard == widget.boardRepository.boardDocument.id,
                        ),
                      )
                    ],
                  ),
                ): new Container(),
                new FlatButton(
                  minWidth: 70.0,
                  padding: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
                  onPressed: (){
                    setState(() {
                      editing = !editing;
                    });
                    FocusScope.of(context).requestFocus(new FocusNode());
                  }, 
                  child: new Text(editing ? "done" : "edit",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                )
              ],
            )
          )
        ],
      )
    );
  }
}
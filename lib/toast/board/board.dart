import 'package:flutter/material.dart';
import 'package:toast/models/board/document.dart';
import 'package:toast/repositories/board.dart';
import 'package:toast/repositories/projects.dart';
import 'package:toast/toast/board/layout.dart';
import 'package:toast/widgets/cards/card.dart';

class BoardList extends StatefulWidget {
  final ProjectRepository projectRepository;
  final List<BoardRepository> boards;
  final bool showTitles;
  final bool showCurrent;
  final bool asDashboard;
  final bool showAddOnEmpty;
  BoardList({this.boards = const[], this.showTitles = true, this.projectRepository, this.showCurrent = true, this.asDashboard = false, this.showAddOnEmpty = false});

  @override
  _BoardListState createState() => _BoardListState();
}

class _BoardListState extends State<BoardList> {
  
  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new CustomScrollView(
      physics: AlwaysScrollableScrollPhysics(),
      slivers: <Widget>[
        (widget?.boards?.length ?? -1) > 0 ? 
        new SliverPadding(
          padding: new EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0, bottom: 100.0),
          sliver: new SliverList(
            delegate: new SliverChildListDelegate.fixed(
              List.generate(widget.boards.length, (index){
                return new Container(
                  margin: new EdgeInsets.only(bottom: 20.0),
                  child: new FloatingCard(
                    child: new BoardLayout(
                      asDashboard: widget.asDashboard,
                      showCurrent: widget.showCurrent,
                      projectRepository: widget.projectRepository ?? null,
                      boardRepository: widget.boards[index]
                    )
                  ),
                );
              })
            )
          ),
        ) : widget.showAddOnEmpty ?
        new SliverPadding(
          padding: new EdgeInsets.only(top: 15.0),
          sliver: new SliverToBoxAdapter(
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                new Image.asset('assets/start.png', width: 200.0,),
                new Container(width: 30.0)
              ],
            )
          )
        ): new SliverToBoxAdapter(child: new Container())
      ],
    );
  }
}


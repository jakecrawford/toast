
import 'package:flutter/material.dart';
import 'package:toast/models/task/data.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/board/action.dart';
import 'package:toast/toast/board/actionLabel.dart';
import 'package:toast/toast/board/labels.dart';
import 'package:toast/toast/textfield/flattext.dart';

class TaskItem extends StatefulWidget {
  final TaskData taskData;
  final Function(TaskData) onChange;
  final bool isEditing;
  final Function onDelete;
  TaskItem({GlobalKey key, @required this.taskData, @required this.onChange, this.isEditing = false, this.onDelete})
    :super(key: key);

  @override
  _TaskItemState createState() => _TaskItemState();
}

class _TaskItemState extends State<TaskItem> {

  FocusNode focusNode;
  TextEditingController textEditingController;
  ActionLabel progressChange;
  ActionLabel priorityChange;

  reportChange(){
    if (textEditingController.text == "") return;
    widget.onChange(
      new TaskData(
        priority: priorityChange == null ? widget.taskData.priority : priorityChange.id, 
        progress: progressChange == null ? widget.taskData.progress : progressChange.id, 
        user: UserRepository.userDocument.profile.name, 
        task: textEditingController.text
      )
    );
  }

  onFocusChange(){
    if (!focusNode.hasFocus){
      reportChange();
    }
  }

  Widget buildPriority(String key){
    return priorityLabels[key];
  }

  Widget buildProgress(String key){
    return progressLabels[key];
  }

  @override
  void initState() {
    focusNode = new FocusNode()..addListener(onFocusChange);
    textEditingController = new TextEditingController(text: widget.taskData.task ?? "Enter text");
   
    super.initState();
  }

  @override
  void dispose() {
    focusNode?.removeListener(onFocusChange);
    textEditingController?.dispose();
    focusNode?.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.only(left: 15.0, right: 20.0, top: 0.0, bottom: 0.0),
      child: new Column(
        children: [
          new Row(
            children: [
              new CircleAvatar(
                radius: 18.0,
                backgroundColor: Color(0XFF2E3340),
                child: new Icon(Icons.person, color: Colors.white, size: 19.0,),
              ),
              new Container(width: 10.0,),
              new Expanded(
                child: new FlatTextField(
                  focusNode: focusNode,
                  textEditingController: textEditingController,
                  onChange: (v){

                  },
                  text: widget.taskData.task ?? "",
                )
              ),
              MediaQuery.of(context).size.width > 800.0 ? new Row(
                children: [
                  new Container(width: 10.0,),
                  new ChipAction(
                    key: new GlobalKey(),
                    actions: progressLabels.entries.map((entry) => entry.value).toList(),
                    selected: buildProgress(widget.taskData.progress),
                    onChange: (ActionLabel value){
                      progressChange = value;
                      reportChange();
                    },
                  ),
                  new Container(width: 10.0,),
                  new ChipAction(
                    key: new GlobalKey(),
                    actions: priorityLabels.entries.map( (entry) => entry.value).toList(),
                    selected: buildPriority(widget.taskData.priority),
                    onChange: (ActionLabel value){
                      priorityChange = value;
                      reportChange();
                    },
                  ),
                  new AnimatedOpacity(
                    opacity: widget.isEditing ? 1.0 :0.0,
                    curve: Curves.easeOutQuint,
                    duration: new Duration(seconds: 1),
                    child: new AnimatedContainer(
                      duration: new Duration(milliseconds: 500),
                      curve: Curves.ease,
                      width: widget.isEditing ? 50.0 : 0.0,
                      child: new ClipRect(
                        clipBehavior: Clip.hardEdge,
                        child: new IconButton(icon: new Icon(Icons.delete_rounded, color: Colors.white,), onPressed: (){
                          widget.onDelete();
                        }),
                      )
                    ),
                  )
                ],
              ): new Container()
            ]
          ),
          MediaQuery.of(context).size.width <= 800.0 ? new Row(
            children: [
              new Container(width: 10.0,),
              new Column(
                children: [
                  new Text("STATUS", style: new TextStyle(color: Color(0XFF636879), fontSize: 10.0)),
                  new Container(height: 10.0,),
                  new ChipAction(
                    actions: progressLabels.entries.map((entry) => entry.value).toList(),
                    selected: buildProgress(widget.taskData.progress),
                    onChange: (ActionLabel value){
                      progressChange = value;
                      reportChange();
                    },
                  ),
                ],
              ),
              new Container(width: 10.0,),
              new Column(
                children: [
                  new Text("PRIORITY", style: new TextStyle(color: Color(0XFF636879), fontSize: 10.0)),
                  new Container(height: 10.0,),
                  new ChipAction(
                    actions: priorityLabels.entries.map((entry) => entry.value).toList(),
                    selected: buildPriority(widget.taskData.priority),
                    onChange: (ActionLabel value){
                      priorityChange = value;
                      reportChange();
                    },
                  ),
                ],
              ),
              new AnimatedOpacity(
                opacity: widget.isEditing ? 1.0 :0.0,
                curve: Curves.easeOutQuint,
                duration: new Duration(seconds: 1),
                child: new AnimatedContainer(
                  duration: new Duration(milliseconds: 500),
                  curve: Curves.ease,
                  width: widget.isEditing ? 50.0 : 0.0,
                  child: new ClipRect(
                    clipBehavior: Clip.hardEdge,
                    child: new IconButton(icon: new Icon(Icons.delete_rounded, color: Colors.white,), onPressed: (){
                      widget.onDelete();
                    }),
                  )
                ),
              )
            ],
          ): new Container()
        ],
      )
    );
  }
}

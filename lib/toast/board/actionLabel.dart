
import 'package:flutter/material.dart';

class ActionLabel extends StatefulWidget {
  final String name;
  final Color color;
  final String id;

  ActionLabel({
    this.color, this.name, this.id
  });

  @override
  _ActionLabelState createState() => _ActionLabelState();
}

class _ActionLabelState extends State<ActionLabel> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: double.infinity, height: 35.0,
      decoration: new BoxDecoration(
        color: widget.color ?? Theme.of(context).primaryColor,
        borderRadius: new BorderRadius.all(Radius.circular(50.0)),
        boxShadow: [
          new BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.2),
            blurRadius: 9.0, spreadRadius: 9.0, offset: new Offset(0.0, 9.0)
          )
        ]
      ),
      child: new Text(widget.name,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }
}
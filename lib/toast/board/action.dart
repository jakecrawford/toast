
import 'package:flutter/material.dart';
import 'package:toast/toast/board/actionLabel.dart';

class ChipAction extends StatefulWidget {
  final List<ActionLabel> actions;
  final ActionLabel selected;
  final Function(ActionLabel) onChange;

  ChipAction({
    @required this.selected, @required this.actions, this.onChange, GlobalKey key,
  }):super(key: key);

  @override
  _ChipActionState createState() => _ChipActionState();
}

class _ChipActionState extends State<ChipAction> {

  @override
  void didUpdateWidget(covariant ChipAction oldWidget) {
    print("UPDATED ");
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.0, height: 35.0,
      alignment: Alignment.center,
      child: new Stack(
        children: [
          new Listener(
            onPointerDown: (_) => FocusScope.of(context).unfocus(),
            child: new DropdownButton(
              key: new GlobalKey(),
              isExpanded: true,
              icon: new Container(),
              underline: new Container(),
              dropdownColor: Theme.of(context).dialogBackgroundColor,
              onTap: (){
                print("TAPPEd");
                print(widget.selected.name.toString());
              },
              onChanged: (int v){
                widget.onChange(widget.actions[v]);
              },
              selectedItemBuilder: (BuildContext context){
                return [
                  new Container(
                    color: Colors.transparent,
                    width: 100.0, height: 35.0,
                    child: widget.selected ?? new Container(),
                  )
                ];
              },  
              items: List.generate(widget.actions.length, (index){
                return new DropdownMenuItem(
                  value: index,
                  child: new Padding(
                    padding: new EdgeInsets.only(bottom: 7.0, top: 7.0, left: 5.0, right: 5.0),
                    child: widget.actions[index],
                  )
                );
              })
            ),
          ),
          new IgnorePointer(
            ignoring: true,
            child: new Container(
              alignment: Alignment.center,
              child: widget.selected ?? new Container()
            )
          )
        ],
      )
    );
  }
}
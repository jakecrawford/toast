
import 'package:flutter/material.dart';
import 'package:toast/toast/board/action.dart';
import 'package:toast/toast/board/actionLabel.dart';
import 'package:toast/toast/board/labels.dart';
import 'package:toast/toast/textfield/flattext.dart';
import 'package:toast/widgets/buttons/rowButton.dart';

class BoardInputGroup {
  final String text;
  final Map<String, ActionLabel> priority;
  final Map<String, ActionLabel> progress;

  BoardInputGroup({
    @required this.text, @required this.priority, @required this.progress,
  });
}

class BoardInput extends StatefulWidget {
  final Function(BoardInputGroup) onConfirm;
  final BoardInputGroup input;

  BoardInput({
    @required GlobalKey key, @required this.onConfirm, @required this.input
  }):super(key: key);
  
  @override
  BoardInputState createState() => BoardInputState();
}

class BoardInputState extends State<BoardInput> {

  String text = "";
  ActionLabel progress = progressLabels["not_started"];
  ActionLabel priority = priorityLabels['low'];

  TextEditingController textEditingController;

  reset(){
    text = "";
    progress = progressLabels["not_started"];
    priority = priorityLabels['low'];
    textEditingController.clear();
    FocusScope.of(context).requestFocus(new FocusNode());
    if (mounted){
      setState(() {
        
      });
    }
  }

  confirm(){
    if (this.text == "") return;
    widget.onConfirm(
      new BoardInputGroup(
        text: this.text, 
        priority: {"selected": priority}, 
        progress: {"selected": progress}, 
      )
    );
  }

  update(newText){
    setState(() {
      text = newText;
    });
  }

  @override
  void initState() {
    textEditingController = new TextEditingController();
    
    super.initState();
  }

  @override
  void dispose() {
    textEditingController?.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: new EdgeInsets.only(left: 15.0, right: 5.0, top: 5.0, bottom: 5.0), 
      decoration: new BoxDecoration(
        color: Theme.of(context).cardColor,
        borderRadius: new BorderRadius.all(Radius.circular(12.0)),
      ),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              new CircleAvatar(
                radius: 18.0,
                backgroundColor: Color(0XFF2E3340),
                child: new Icon(Icons.person, color: Colors.white, size: 19.0,),
              ),
              new Container(width: 10.0,),
              new Expanded(
                child: new Column(
                  children: [
                    new FlatTextField(
                      textEditingController: textEditingController,
                      hint: "Add a new task",
                      onChange: update,
                      onSubmit: (v){
                        update(v);
                        confirm();
                      },
                      text: "enter new task",
                    ),
                  ]
                ),
              ),
              MediaQuery.of(context).size.width > 800.0 ?
              new Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new AnimatedOpacity(
                    opacity: text == "" ? 0.0 : 1.0,
                    duration: new Duration(milliseconds: 500),
                    child: new RowButton(
                      title: "CONFIRM",
                      onPressed: (){
                        confirm();
                      },
                    ),
                  ),
                  new ChipAction(
                    actions: progressLabels.entries.map((entry) => entry.value).toList(),
                    selected: progress,
                    onChange: (ActionLabel i){
                      setState(() {
                        progress = i;
                      });
                    },
                  ),
                  new Container(width: 10.0,),
                  new ChipAction(
                    actions: priorityLabels.entries.map((entry) => entry.value).toList(),
                    selected: priority,
                    onChange: (ActionLabel i){
                      setState(() {
                        priority = i;
                      });
                    },
                  ),
                  new Container(width: 15.0,),
                ],
              ): new Container()
            ]
          ),
          MediaQuery.of(context).size.width <= 800.0 ? new Padding(
            padding: new EdgeInsets.only(top: 5.0, bottom: 5.0),
            child: new Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new ChipAction(
                  actions: progressLabels.entries.map((entry) => entry.value).toList(),
                  selected: progress,
                  onChange: (ActionLabel i){
                    setState(() {
                      progress = i;
                    });
                  },
                ),
                new Container(width: 10.0,),
                new ChipAction(
                  actions: priorityLabels.entries.map((entry) => entry.value).toList(),
                  selected: priority,
                  onChange: (ActionLabel i){
                    setState(() {
                      priority = i;
                    });
                  },
                ),
                new Expanded(
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      new IconButton(
                        icon: Icon(Icons.send_rounded, color: Theme.of(context).primaryColor,),
                        onPressed: (){
                          confirm();
                        },
                      )
                    ],
                  )
                
                )
              ],
            ),
          ): new Container()
        ],
      )
    );
  }
}
import 'package:flutter/material.dart';
import 'package:toast/toast/board/actionLabel.dart';

Map <String, ActionLabel> priorityLabels = {
  "low": new ActionLabel(
    name: "Low",
    id: "low",
    color: Color(0XFF038387),
  ),
  "normal": new ActionLabel(
    name: "Normal",
    id: "normal",
    color: Color(0XFF0D8AEE),
  ),
  "high": new ActionLabel(
    name: "High",
    id: "high",
    color: Color(0XFF2F50BE),
  ),
  "critical": new ActionLabel(
    name: "Critical",
    id: "critical",
    color: Color(0XFFED3C39),
  ),
  "nice_to_have": new ActionLabel(
    name: "Nice to Have",
    id: "nice_to_have",
    color: Color(0XFF00BBD3),
  )
};

Map <String, ActionLabel> progressLabels = {
  "not_started": new ActionLabel(
    name: "Not Started",
    id: "not_started",
    color: Color(0XFF575C6A),
  ),
  "stalled": new ActionLabel(
    name: "Stalled",
    id: "stalled",
    color: Color(0XFFFFAB00),
  ),
  "started": new ActionLabel(
    name: "Started",
    id: "started",
    color: Color(0XFF0D8AEE),
  ),
  "almost_done": new ActionLabel(
    name: "Almost Done",
    id: "almost_done",
    color: Color(0XFF00BBD3),
  ),
  "done": new ActionLabel(
    name: "Done",
    id: "done",
    color: Color(0XFF038387),
  ),
  "review": new ActionLabel(
    name: "Review",
    id: "review",
    color: Color(0XFF4A5AB9),
  ),
  "approved": new ActionLabel(
    name: "Approved",
    id: "approved",
    color: Color(0XFF2F50BE),
  ),
  "live": new ActionLabel(
    name: "Live",
    id: "live",
    color: Color(0XFFED3C39),
  ),
  "deprecated": new ActionLabel(
    name: "Deprecated",
    id: "deprecated",
    color: Color(0XFF242834),
  )
};
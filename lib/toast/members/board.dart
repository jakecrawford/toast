

import 'package:flutter/material.dart';
import 'package:toast/models/friend/data.dart';
import 'package:toast/models/member/document.dart';
import 'package:toast/models/project/data.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/friends/friendItem.dart';
import 'package:toast/widgets/cards/card.dart';

class MembersBoard extends StatefulWidget {
  final List<MemberDocument> users;
  final String emptyString;
  final Function onInvite;
  MembersBoard({@required this.users, this.emptyString = "No team members", this.onInvite});

  @override
  _MembersBoardState createState() => _MembersBoardState();
}

class _MembersBoardState extends State<MembersBoard> {

  @override
  void initState() {

    super.initState();
  }

  @override
  void dispose() {

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new SingleChildScrollView(
      physics: AlwaysScrollableScrollPhysics(),
      child: new Padding(
        padding: new EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
        child: new FloatingCard(
          child: AnimatedContainer(
          duration: new Duration(milliseconds: 500),
          curve: Curves.ease,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new Padding(
                  padding: new EdgeInsets.only(top: 20.0, left: 20.0),
                  child: new Text("All",
                    style: new TextStyle(
                      color: Theme.of(context).primaryColor, fontFamily: "Nunito", fontWeight: FontWeight.w700, fontSize: 14.0
                    ),
                  ),
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 5.0, left: 20.0, bottom: 10.0),
                  child: new Text("Team",
                    style: new TextStyle(
                      color: Colors.white, fontFamily: "Nunito", fontWeight: FontWeight.w700, fontSize: 32.0
                    ),
                  ),
                ),
                widget.users.length == 0 ? new Container(
                  child: new Padding(
                    padding: new EdgeInsets.only(left: 20.0, bottom: 10.0),
                    child: new Text(widget.emptyString),
                  )
                ):
                new Column(
                  children: List.generate(widget.users.length, (index){
                    return new Padding(
                      padding: new EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10.0),
                      child: new FriendItem(
                        key: new GlobalKey(),
                        isFriend: true,
                        friendData: new FriendData(
                          name: widget.users[index].memberData.name,
                          uid: widget.users[index].memberData.uid
                        ),
                        onPressed: (v){

                        },
                      ),
                    );
                  }),
                ),
                new Container(height: 10.0),
                new GestureDetector(
                  onTap: (){
                    widget.onInvite();
                  },
                  child: new Container(
                    padding: new EdgeInsets.only(left: 20.0, right: 20.0, top: 17.0, bottom: 17.0), 
                    decoration: new BoxDecoration(
                      color: Theme.of(context).cardColor,
                      borderRadius: new BorderRadius.all(Radius.circular(12.0)),
                    ),
                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        new Expanded(
                          child: new Column(
                            children: [
                              new Text("Invite Friend")
                            ]
                          ),
                        ),
                      ]
                    )
                  )
                )
              ]
            ),
          )
        )
      ),
    );
  }
}
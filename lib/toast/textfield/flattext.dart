
import 'package:flutter/material.dart';

class FlatTextField extends StatefulWidget {
  final String text;
  final String hint;
  final bool autoCorrect;
  final bool autoFocus;
  final FocusNode focusNode;
  final Function(String) onChange;
  final Function(String) onSubmit;
  final TextEditingController textEditingController;
  final InputDecoration inputDecoration;
  final TextInputType textInputType;
  final TextCapitalization textCapitalization;

  FlatTextField({
    @required this.onChange, this.text = "", this.hint = "", this.textEditingController, this.onSubmit, this.inputDecoration, this.autoFocus = false, this.focusNode, this.textInputType, this.autoCorrect, this.textCapitalization
  });

  @override
  _FlatTextFieldState createState() => _FlatTextFieldState();
}

class _FlatTextFieldState extends State<FlatTextField> {

  TextEditingController textEditingController;

  @override
  void initState() {
    if (widget.textEditingController == null){
      textEditingController = new TextEditingController(text: widget.text); 
    }
    
    super.initState();
  }

  @override
  void dispose() {
    textEditingController?.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: double.infinity,
      child: new TextField(
        onChanged: (String change){
          widget.onChange(change);
        },
        onSubmitted: (change){
          widget.onSubmit(change);
        },
        controller: widget.textEditingController == null ? textEditingController : widget.textEditingController, 
        autocorrect: widget.autoCorrect ?? true, 
        focusNode: widget.focusNode ?? null,
        autofocus: widget.autoFocus,
        keyboardAppearance: Theme.of(context).brightness, 
        keyboardType: widget.textInputType ?? TextInputType.text, 
        cursorColor: Theme.of(context).primaryColor, 
        textCapitalization: widget.textCapitalization ?? TextCapitalization.sentences,
        maxLines: null, 
        cursorRadius: Radius.circular(3.0), 
        cursorWidth: 3.0,
        style: Theme.of(context).textTheme.bodyText2,
        decoration: widget.inputDecoration ?? new InputDecoration(
          hintStyle: Theme.of(context).textTheme.bodyText2,
          hintText: widget.hint,
          border: new UnderlineInputBorder(
            borderSide: BorderSide.none,
          )
        )
      )
    );
  }
}
import 'package:flutter/material.dart';
import 'package:toast/models/user/document.dart';

class UserCircle extends StatefulWidget {
  final UserDocument userDocument;
  final bool showNotification;
  final Function onPressed;
  final Color color;

  UserCircle({
    this.userDocument, this.showNotification = false, this.onPressed, this.color
  });

  @override
  _UserCircleState createState() => _UserCircleState();
}

class _UserCircleState extends State<UserCircle> {

  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      onTap: (){
        widget?.onPressed();
      },
      child: Container(
        color: Colors.transparent,
        child: new Row(
          children: [
            new Stack(
              alignment: Alignment.center,
              children: [
                new CircleAvatar(
                  radius: 16.0,
                  backgroundColor: widget.color ?? Theme.of(context).primaryColor,
                  child: new Icon(Icons.person, color: Colors.white, size: 18.0,),
                ),
                new Positioned(
                  right: 0.0, top: 0.0,
                  child: widget.showNotification ? new Container(
                    width: 9.0, height: 9.0,
                    decoration: new BoxDecoration(
                      color: Colors.redAccent,
                      shape: BoxShape.circle
                    ),
                  ): new Container()
                )
              ],
            ),
            new Container(width: 12.0),
            new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new Text(widget?.userDocument?.profile?.name ?? "User", style: Theme.of(context).textTheme.headline5,),
                new Transform(
                  transform: new Matrix4.translationValues(-0.0, -2.0, 0.0),
                  child: new Text("Developer", style: Theme.of(context).textTheme.bodyText1,),
                )
              ]
            )
          ]
        )
      ),
    );
  }
}
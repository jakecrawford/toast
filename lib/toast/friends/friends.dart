
import 'package:flutter/material.dart';
import 'package:toast/models/friend/data.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/friends/friendItem.dart';

class FriendBoard extends StatefulWidget {
  final List<FriendData> users;
  final String emptyString;
  FriendBoard({@required this.users, this.emptyString = "You have no friends"});

  @override
  _FriendBoardState createState() => _FriendBoardState();
}

class _FriendBoardState extends State<FriendBoard> {

  @override
  void initState() {

    super.initState();
  }

  @override
  void dispose() {

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: new Duration(milliseconds: 500),
      curve: Curves.ease,
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          new Padding(
            padding: new EdgeInsets.only(top: 20.0, left: 20.0),
            child: new Text("All",
              style: new TextStyle(
                color: Theme.of(context).primaryColor, fontFamily: "Nunito", fontWeight: FontWeight.w700, fontSize: 14.0
              ),
            ),
          ),
          new Padding(
            padding: new EdgeInsets.only(top: 5.0, left: 20.0, bottom: 10.0),
            child: new Text("My Friends",
              style: new TextStyle(
                color: Colors.white, fontFamily: "Nunito", fontWeight: FontWeight.w700, fontSize: 32.0
              ),
            ),
          ),
          widget.users.length == 0 ? new Container(
            child: new Padding(
              padding: new EdgeInsets.only(left: 20.0, bottom: 10.0),
              child: new Text(widget.emptyString),
            )
          ):
          new Column(
            children: List.generate(widget.users.length, (index){
              return new Padding(
                padding: new EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10.0),
                child: new FriendItem(
                  key: new GlobalKey(),
                  isFriend: true,
                  friendData: widget.users[index],
                  onPressed: (v){

                  },
                ),
              );
            }),
          ),
          new Container(height: 10.0),
        ]
      ),
    );
  }
}
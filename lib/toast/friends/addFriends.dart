
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:toast/models/friend/data.dart';
import 'package:toast/models/request/data.dart';
import 'package:toast/models/user/data.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/dialog/dialog.dart';
import 'package:toast/toast/friends/confirmAdd.dart';
import 'package:toast/toast/friends/confirmDelete.dart';
import 'package:toast/toast/friends/friendItem.dart';
import 'package:toast/widgets/appbar/folding.dart';
import 'package:toast/widgets/searchbar/simple.dart';

class AddFriends extends StatefulWidget {
  final ScrollController scrollController;
  final Function onUpdate;
  AddFriends({@required this.scrollController, @required this.onUpdate});

  @override
  _AddFriendsState createState() => _AddFriendsState();
}

class _AddFriendsState extends State<AddFriends> {

  List<FriendData> myFriends = [];
  String query = "";

  bool hasFriend(FriendData friendData){
    bool exists = false;
    for (var i = 0; i < myFriends.length; i++) {
      if (myFriends[i].uid == friendData.uid){
        exists = true;
        return exists;
      }
    }
    return exists;
  }

  addFriend(FriendData data){
    myFriends.add(data);
    UserRepository.requests.sendRequest(
      new RequestData(
        name: UserRepository.userDocument.profile.name, 
        type: "friendRequest", 
        receiver: data.uid, 
        sender: FirebaseAuth.instance.currentUser.uid, 
        identifier: FirebaseAuth.instance.currentUser.uid
      )
    );
    confirmTransaction();
  }

  removeFriend(FriendData data){
    for (var i = 0; i < myFriends.length; i++) {
      if (myFriends[i].uid == data.uid){
        myFriends.removeAt(i);
      }
    }
    confirmTransaction();
  }

  confirmTransaction(){
    UserData user = UserRepository.userDocument.profile;
    user.friends = myFriends;
    UserRepository.updateUser(user);
    widget.onUpdate();
    setState(() {
      
    });
  }

  promptAction(FriendData data){
    ToastDialog.show(
      context: context,
      child: !hasFriend(data) ? 
        new ConfirmAdd(
          friend: data,
          onConfirm: (){
            Navigator.of(context).pop();
            addFriend(data);
          },
        ): 
        new ConfirmDelete(
          friend: data,
          onConfirm: (){
            removeFriend(data);
            Navigator.of(context).pop();
          },
        )
    );

  }

  @override
  void initState() {
    myFriends = UserRepository.userDocument?.profile?.friends ?? [];

    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return new CustomScrollView(
      controller: widget?.scrollController,
      slivers: [
        new SliverPersistentHeader(
          pinned: false, floating: true,
          delegate: new FoldingAppBarDelegate(
            height: 100.0,
            child: new Container(
              child: new SimpleSearchBar(
                onChange: (v){
                  setState(() {
                    print(v);
                    query = v;
                  });
                },
              ),
            )
          )
        ),
        new StreamBuilder(
          stream: FirebaseFirestore.instance.collection("Users").where("name", isGreaterThanOrEqualTo: query)
            .where("name", isLessThanOrEqualTo: query + '~').snapshots(),
          builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
            if (snapshot.hasData){
              return new SliverList(
                delegate: new SliverChildBuilderDelegate(
                  (BuildContext context, int i){
                    FriendData friend = FriendData.fromJson(snapshot.data.docs[i].data());
                    if (FirebaseAuth.instance.currentUser.uid == friend.uid){
                      return new Container(key: new GlobalKey());
                    }
                    return new Padding(
                      padding: new EdgeInsets.only(left: 20.0, right: 20.0,),
                      child: new FriendItem(
                        editing: true,
                        key: new GlobalKey(),
                        friendData: friend,
                        isFriend: hasFriend(friend),
                        onPressed: (FriendData c){
                          promptAction(c);
                        },
                      )
                    );
                  },
                  childCount: (snapshot.data?.docs?.length ?? 0) > 10 ? 10 : (snapshot.data?.docs?.length ?? 0)
                ),
              );
            } else {
              return new SliverToBoxAdapter(
                child: new Padding(
                  padding: new EdgeInsets.only(top: 70.0, bottom: 70.0),
                  child:new Column(
                    children: [
                      new Container(
                        width: 100.0, height: 100.0,
                        child: new CircularProgressIndicator(
                          valueColor: new AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
                        ),
                      ),
                    ],
                  )
                )
              );
            }
          }
        )
      ],
    );
  }
}

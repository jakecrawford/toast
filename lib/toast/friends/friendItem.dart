
import 'package:flutter/material.dart';
import 'package:toast/models/friend/data.dart';
import 'package:toast/widgets/buttons/chipButton.dart';

class FriendItem extends StatefulWidget {
  final FriendData friendData;
  final Function(FriendData) onPressed; 
  final bool isFriend; 
  final bool editing;

  FriendItem({
    @required GlobalKey key,  @required this.friendData, this.isFriend = false, @required this.onPressed, this.editing = false
  }): super(key: key);
  
  @override
  _FriendItemState createState() => _FriendItemState();
}

class _FriendItemState extends State<FriendItem> {
  
  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          new Row(
            children: [
              new CircleAvatar(
                radius: 18.0,
                backgroundColor: Color(0XFF2E3340),
                child: new Icon(Icons.person, color: Colors.white, size: 19.0,),
              ),
              new Container(width: 10.0,),
              new Text(widget?.friendData?.name ?? "Unknown"),
            ],
          ),
          widget.editing ? new Container(
            child: widget.isFriend ? 
              new IconButton(icon: Icon(Icons.delete_rounded), onPressed: (){
                widget.onPressed(widget.friendData);
              })

            :new ChipButton(
              padding: new EdgeInsets.only(left: 5.0, right: 5.0, top: 2.0, bottom: 2.0),
              onTap: (){
                widget.onPressed(widget.friendData);
              }, 
              text: "Add Friend"
            ),
          ): new Container()
        ],
      ),
    );
  }
}
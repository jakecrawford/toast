

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:toast/models/friend/data.dart';
import 'package:toast/models/project/data.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/picker/colorPicker.dart';
import 'package:toast/toast/picker/iconPicker.dart';
import 'package:toast/views/dashboard.dart';
import 'package:toast/widgets/buttons/rowButton.dart';

class ConfirmDelete extends StatefulWidget {
  final Function onConfirm;
  final FriendData friend;

  ConfirmDelete({
    @required this.onConfirm, @required this.friend
  });

  @override
  _ConfirmDeleteState createState() => _ConfirmDeleteState();
}

class _ConfirmDeleteState extends State<ConfirmDelete> {
  
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.transparent,
      body: new Container(
        width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height,
        child: new SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: new Column(
            children: [
              new Container(height: MediaQuery.of(context).size.height*0.1),
              new AnimatedContainer(
                width: 600.0,
                curve: Curves.easeOutExpo,
                duration: new Duration(seconds: 1),
                padding: new EdgeInsets.only(),
                decoration: new BoxDecoration(
                  color: Theme.of(context).cardColor,
                  borderRadius: new BorderRadius.all(Radius.circular(5.0)),
                ),
                child: new Stack(
                  alignment: Alignment.center,
                  children: [
                    new Column(
                      children: [
                        new Padding(
                          padding: new EdgeInsets.only(top: 20.0, bottom: 10.0),
                          child: new Text(widget?.friend?.name ?? "Unknown",
                            style: Theme.of(context).textTheme.headline2, 
                          )
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(top: 10.0),
                          child: new IgnorePointer(
                            ignoring: true,
                            child: new IconPicker(
                              showeEditButton: false,
                              iconColor: Theme.of(context).primaryColor,
                              selected: Icons.person,
                              onChange: (int i){
                                
                              },
                            ),
                          )
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(top: 30.0),
                          child: new Text("Would you like to remove " + widget?.friend?.name + " as a friend?",
                            style: Theme.of(context).textTheme.headline5, 
                          )
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(right: 0.0),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              new RowButton(title: "REMOVE FRIEND", onPressed: (){
                                widget.onConfirm();
                              }),
                            ],
                          )
                        )
                      ],
                    ),
                    new Positioned(
                      top: 0.0, right: 0.0,
                      child: new CloseButton(
                      
                      ),
                    )
                  ],
                )
              )
            ],
          ),
        ),
      ),
    );
  }
}


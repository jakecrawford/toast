
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:toast/models/request/data.dart';
import 'package:toast/models/request/document.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/dialog/dialog.dart';
import 'package:toast/toast/user/circle.dart';
import 'package:toast/views/dashboard.dart';
import 'package:toast/views/myProfile.dart';

class ToastAppBar extends StatefulWidget {
  final Widget leading;
  final Widget action;
  final Widget primary;
  final String title;
  final Color userColor;

  ToastAppBar({
    @required this.action, @required this.leading, @required this.title, @required this.primary, this.userColor
  });

  @override
  _ToastAppBarState createState() => _ToastAppBarState();
}

class _ToastAppBarState extends State<ToastAppBar> {

  List<RequestDocument> requests = [];

  update(){
    requests = UserRepository.requests.requests ?? [];
    if (mounted){
      setState(() {
        
      });
    }
  }

  check(){
    try {
      requests = UserRepository.requests.requests;
    } catch(e){

    }
  }

  @override
  void initState() {
    UserRepository?.requests?.addListener(update);
    check();

    super.initState();
  }

  @override
  void dispose() {
    UserRepository?.requests?.removeListener(update);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Builder(
      builder: (BuildContext cxt){
        return new Container(
          color: Theme.of(context).backgroundColor,
          padding: new EdgeInsets.only(left: MediaQuery.of(context).padding.left/2, right: MediaQuery.of(context).padding.right/2),
          child: new Column(
            children: [
              new Container(height: MediaQuery.of(context).padding.top > 0.0 ? MediaQuery.of(context).padding.top : 5.0),
              new Expanded(
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    new Row(
                      children: [
                        widget.primary,
                        new Text(widget.title ?? "Dashboard", style: Theme.of(context).textTheme.headline5,)
                      ],
                    ),
                    new Row(
                      children: [
                        new Padding(
                          padding: new EdgeInsets.only(right: 20.0, left: 4.0),
                          child: new UserCircle(
                            showNotification: requests.length > 0,
                            userDocument: UserRepository.userDocument,
                            color: widget.userColor,
                            onPressed: (){
                              ToastDialog.show(
                                context: context,
                                child: new MyProfile(
                                  onConfirm: (){
                                    Navigator.pushAndRemoveUntil(context,
                                      MaterialPageRoute(builder: (context) => new DashboardView(),), 
                                      (Route<dynamic> route) => false
                                    );
                                  },
                                )
                              );
                            },
                          ),
                        )
                      ],
                    )
                  ]
                ),
              ),
              new Expanded(
                child: new Container(
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      new Expanded(
                        child: widget.leading,
                      ),
                      widget.action
                    ],
                  ),
                )
              ),
              new Container(height: MediaQuery.of(context).padding.top > 0.0 ? 0.0 : 5.0)
            ]
          ),
        );
      }
    );
  }
}
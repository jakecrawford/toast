
import 'package:flutter/material.dart';

class ToastDialog {
  static show({
    BuildContext context, Widget child
  }){
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return child;
      }
    );
  }
}

import 'package:flutter/material.dart';
import 'package:toast/models/project/data.dart';
import 'package:toast/models/project/document.dart';
import 'package:toast/models/request/document.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/dialog/dialog.dart';
import 'package:toast/toast/drawer/layout.dart';
import 'package:toast/toast/picker/list.dart';
import 'package:toast/views/dashboard.dart';
import 'package:toast/views/manage.dart';
import 'package:toast/views/myProfile.dart';
import 'package:toast/views/project.dart';

class DrawerPages {

  static getIcon(int len, bool selected){
    print(len);
    if (len > 0){
      return new Stack(
        alignment: Alignment.center,
        children: [
          new Icon(Icons.notifications_rounded, color: Color(0XFF575C6A),),
          new Positioned(
            right: 0.0, top: 0.0,
            child: new Container(
              width: 9.0, height: 9.0,
              decoration: new BoxDecoration(
                color: Colors.redAccent,
                shape: BoxShape.circle
              ),
            )
          )
        ],
      );
    } else {
      return new Icon(Icons.notifications_rounded, color: Color(0XFF575C6A),);
    }
  }

  static List<DrawerItem>generatePages(BuildContext context, String selected){
    List<DrawerItem> pages = [
      new DrawerItem(
        name: "Home", 
        icon: new Icon(Icons.home_rounded, color: selected == "home" ? Colors.white : Theme.of(context).primaryColor),
        isSelected: selected == "home", 
        onPressed: (){
          Navigator.pushAndRemoveUntil(context,
            MaterialPageRoute(builder: (context) => new DashboardView(),), 
            (Route<dynamic> route) => false
          );
        }, 
      ),
      new DrawerItem(
        name: "Manage", 
        icon: new Icon(Icons.people_alt_rounded, color: selected == "manage" ? Colors.white : Colors.teal,),
        isSelected: selected == "manage", 
        onPressed: (){
          Navigator.pushAndRemoveUntil(context,
            MaterialPageRoute(builder: (context) => new ManagePage(),), 
            (Route<dynamic> route) => false
          );
        }, 
      ),
      new DrawerItem(
        name: "Notifications", 
        icon: getIcon(UserRepository.requests.requests.length, selected == "notifications"),
        isSelected: selected == "notifications", 
        onPressed: (){
          ToastDialog.show(
            context: context,
            child: new MyProfile(
              onConfirm: (){
                Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) => new DashboardView(),), 
                  (Route<dynamic> route) => false
                );
              },
            )
          );
        }, 
      ),
    ];

    UserRepository.projects.forEach((repository) {
      pages.add(
        new DrawerItem(
          name: repository?.project?.data?.name ?? "Unkown", 
          icon: new Icon(myIcons[repository?.project?.data?.iconIndex ?? 0], 
            color: selected == (repository?.project?.data?.name ?? "null") ? Colors.white: myColors[repository?.project?.data?.colorIndex ?? 0],
          ),
          isSelected: selected == (repository?.project?.data?.name ?? "null"), 
          onPressed: (){
            Navigator.pushAndRemoveUntil(context,
              MaterialPageRoute(builder: (context) => new UniqueProjectView(
                projectRepository: repository,
              ),), 
              (Route<dynamic> route) => false
            );
          }, 
        )
      );
    });

    return pages;
  }
}
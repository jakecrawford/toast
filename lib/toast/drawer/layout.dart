import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/views/login.dart';
import 'package:toast/widgets/appbar/folding.dart';

class DrawerLayout extends StatefulWidget {
  final List<DrawerItem> pages;
  DrawerLayout({@required this.pages});

  @override
  _DrawerLayoutState createState() => _DrawerLayoutState();
}

class _DrawerLayoutState extends State<DrawerLayout> {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: 300.0,
      color: Theme.of(context).backgroundColor,
      child: new CustomScrollView(
        slivers: [
          new DrawerHeader(),
          new SliverFillRemaining(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                new Column(
                  children: List.generate(widget.pages.length, (index) => widget.pages[index]),
                ),
                new Padding(
                  padding: new EdgeInsets.only(bottom: 20.0),
                  child: new FlatButton(
                    onPressed: (){
                      FirebaseAuth.instance.signOut();
                      UserRepository.reset();
                      print("logout");
                      Navigator.pushAndRemoveUntil(context,
                        MaterialPageRoute(builder: (context) => new ToastLogin()), 
                        (Route<dynamic> route) => false
                      );
                    },
                    child: new Text("Logout"),
                  ),
                )
              ],
            )
          )
        ],
      ),
    );
  }
}

class DrawerItem extends StatefulWidget {
  final String name;
  final Widget icon;
  final bool isSelected;
  final Function onPressed;

  DrawerItem({
    @required this.name, @required this.isSelected, @required this.onPressed, @required this.icon
  });

  @override
  _DrawerItemState createState() => _DrawerItemState();
}

class _DrawerItemState extends State<DrawerItem> {
  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      onTap: (){
        widget.onPressed();
      },
      child: Container(
        width: double.infinity,
        color: !widget.isSelected ? Colors.transparent : Theme.of(context).primaryColor,
        padding: new EdgeInsets.only(top :10.0, bottom: 10.0, left: 10.0),
        child: new Row(
          children: [
            new Container(
              width: 20.0,
              child: widget.icon ?? new Container(),
            ),
            new Container(width: 10.0,),
            new Text(widget.name, style: Theme.of(context).textTheme.headline5,),
          ],
        )
      ),
    );
  }
}

class DrawerHeader extends StatefulWidget {
  @override
  _DrawerHeaderState createState() => _DrawerHeaderState();
}

class _DrawerHeaderState extends State<DrawerHeader> {
  double height = 60.0;

  @override
  Widget build(BuildContext context) {
    return new SliverPersistentHeader(
      pinned: false, floating: true,
      delegate: new FoldingAppBarDelegate(
        height: height + MediaQuery.of(context).padding.top,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            new Container(
              height: height,
              alignment: Alignment.center,
              padding: new EdgeInsets.only(left: 20.0),
              child: new Row(
                children: [
                  new Image.asset('assets/logo.png', width: 70.0,)
                ]
              ),
            )
          ]
        )
      )
    );
  }
}
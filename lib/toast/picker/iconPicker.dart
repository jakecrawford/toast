
import 'package:flutter/material.dart';
import 'package:toast/toast/picker/list.dart';

class IconPicker extends StatefulWidget {
  final Size size;
  final bool showeEditButton;
  final Color iconColor;
  final Function(int) onChange;
  final IconData selected;

  IconPicker({this.size, this.showeEditButton = false, this.iconColor, this.onChange, this.selected});

  @override
  _IconPickerState createState() => _IconPickerState();
}

class _IconPickerState extends State<IconPicker> {

  showIcons(){
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Theme.of(context).backgroundColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      builder: (_) {
        return DraggableScrollableSheet(
          initialChildSize: 0.75,
          expand: false,
          builder: (_, controller) {
            return new CustomScrollView(
              controller: controller,
              slivers: [
                new SliverGrid(
                  gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 6
                  ),
                  delegate: new SliverChildBuilderDelegate(
                    (BuildContext cont, int index){
                      return new GestureDetector(
                        onTap: (){
                          widget.onChange(index);
                          Navigator.of(context).pop();
                        },
                        child: new Container(
                          decoration: new BoxDecoration(
                            color: widget?.selected == myIcons[index] ? Theme.of(context).cardColor : Colors.transparent,
                            borderRadius: BorderRadius.all(Radius.circular(10.0))
                          ),
                          child: new Icon(
                            myIcons[index], color: widget.iconColor ?? Colors.white,
                            size: MediaQuery.of(context).size.width*0.08,
                          ),
                        ),
                      );
                    },
                    childCount: myIcons.length
                  ),
                ),
              ],
            );
          }
        );
      }
    );
  }

  @override
  void initState() {
    
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      onTap: (){
        showIcons();
      },
      child: new Container(
        color: Colors.transparent,
        child: new Stack(
          alignment: Alignment.center,
          children: [
            new Container(
              width: 150.0, height: 150.0,
              decoration: new BoxDecoration(
                shape: BoxShape.circle,
                color: Color(0XFF464A55)
              ),
              child: new Icon(widget.selected ?? myIcons[0], size: 100.0, color: widget.iconColor ?? Colors.white,),
            ),
            widget.showeEditButton ? 
            new Positioned(
              right: 5.0, bottom: 5.0,
              child: new Container(
                width: 40.0, height: 40.0,
                decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  color: Theme.of(context).primaryColor
                ),
                child: new Icon(Icons.add_a_photo_rounded, size: 15.0,),
              ),
            ): new Container()
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class ColorPicker extends StatefulWidget {
  final List<Color> colors;
  final Function(int) onSelected;
  final int columns;

  ColorPicker({
    @required this.colors, @required this.columns, @required this.onSelected
  });

  @override
  _ColorPickerState createState() => _ColorPickerState();
}

class _ColorPickerState extends State<ColorPicker> {

  int selected = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: (widget.columns ?? 7)*50.0,
      child: new Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        runAlignment: WrapAlignment.center,
        alignment: WrapAlignment.center,
        children: List.generate(widget.colors.length, (index){
          return new GestureDetector(
            onTap: (){
              setState(() {
                selected = index;
              });
              widget.onSelected(index);
            },
            child: new ColorCircle(color: widget.colors[index], selected: selected == index),
          );
        })
      ),
    );
  }
}

class ColorCircle extends StatefulWidget {
  final bool selected;
  final Color color;

  ColorCircle({
    @required this.color, @required this.selected,
  });

  @override
  _ColorCircleState createState() => _ColorCircleState();
}

class _ColorCircleState extends State<ColorCircle> {
  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: new Duration(milliseconds: 300),
      width: 30.0, height: 30.0,
      margin: new EdgeInsets.all(5.0),
      decoration: new BoxDecoration(
        color: widget.color,
        shape: BoxShape.circle,
        border: widget.selected ? new Border.all(
          color: Colors.white,
          width: 2.5
        ): null
      ),
    );
  }
}
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:toast/views/splashscreen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final FirebaseApp _initialization = await Firebase.initializeApp(
    name: "Toast",
    options: FirebaseOptions(
      appId: "1:209103675749:ios:bd06515ba09f529854018c",
      messagingSenderId: "209103675749",
      apiKey: "AIzaSyBKs3lfxyOPALNJXJpUeKveSXaCaiOUEFg",
      projectId: "toast-e8064"
    )
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  final FirebaseAnalytics analytics = FirebaseAnalytics();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Color(0XFF2F50BE),
        backgroundColor: Color(0XFF15171E),
        scaffoldBackgroundColor: Color(0XFF0A0C10),
        dialogBackgroundColor: Color(0XFF1C1E25),
        cardColor: Color(0XFF202229),
        textSelectionColor: Color(0XFF2F50BE),
        textSelectionHandleColor: Color(0XFF2F50BE),
        fontFamily: "Nunito",
        textTheme: TextTheme(
          button: new TextStyle(color: Color(0XFF2F50BE), fontSize: 14.0, fontWeight: FontWeight.w700),
          headline1: new TextStyle(color: Color(0XFFE3EAEF), fontSize: 32.0, fontWeight: FontWeight.w700, fontFamily: "Nunito"),
          headline2: new TextStyle(color: Color(0XFFE3EAEF), fontSize: 22.0, fontWeight: FontWeight.w700, fontFamily: "Nunito"),
          headline3: new TextStyle(color: Color(0XFFE3EAEF), fontSize: 16.0, fontWeight: FontWeight.w600, fontFamily: "Nunito"),
          headline4: new TextStyle(color: Color(0XFFE3EAEF), fontSize: 15.0, fontWeight: FontWeight.w600, fontFamily: "Nunito"),
          headline5: new TextStyle(color: Color(0XFFE3EAEF), fontSize: 13.0, fontWeight: FontWeight.w600, fontFamily: "Nunito"),
          headline6: new TextStyle(color: Color(0XFFE3EAEF), fontSize: 12.0, fontWeight: FontWeight.w600, fontFamily: "Nunito"),
          bodyText1: new TextStyle(color: Color(0XFFE3EAEF), fontSize: 12.0, fontWeight: FontWeight.w100, fontFamily: "Nunito"),
          bodyText2: new TextStyle(color: Color(0XFFE3EAEF), fontSize: 13.0, fontWeight: FontWeight.w700),
        )
      ),
      home: new SplashScreen()
    );
  }
}

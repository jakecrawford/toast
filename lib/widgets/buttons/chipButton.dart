import 'package:flutter/material.dart';

class ChipButton extends StatefulWidget {
  final Function onTap;
  final String text;
  final EdgeInsets padding;
  final Color color;

  ChipButton({
    @required this.onTap, @required this.text, this.padding, this.color
  });

  @override
  _ChipButtonState createState() => _ChipButtonState();
}

class _ChipButtonState extends State<ChipButton> {
  @override
  Widget build(BuildContext context) {
    return new RawChip(
      onPressed: (){
        widget.onTap();
      },
      backgroundColor: widget.color ?? Theme.of(context).primaryColor,
      label: new Container(
        padding: widget.padding ?? new EdgeInsets.symmetric(
          horizontal: 80.0, vertical: 10.0
        ),
        child: new Text(widget.text,
          style: Theme.of(context).textTheme.headline5,
        ),
      ),
    );
  }
}
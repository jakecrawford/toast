
import 'package:flutter/material.dart';

class RowButton extends StatefulWidget {
  final String title;
  final Function onPressed;
  final Widget icon;
  final Color color;

  RowButton({
    @required this.title, @required this.onPressed, this.icon, this.color
  });

  @override
  _RowButtonState createState() => _RowButtonState();
}

class _RowButtonState extends State<RowButton> {
  
  @override
  Widget build(BuildContext context) {
    return new FlatButton(
      onPressed: (){
        widget.onPressed();
      }, 
      child: new Row(
        children: [
          new Text(widget.title, style: Theme.of(context).textTheme.headline6,),
          new Container(width: 10.0,),
          new Container(
            alignment: Alignment.center,
            width: 22.0, height: 22.0,
            decoration: new BoxDecoration(
              color: widget.color ?? Theme.of(context).primaryColor,
              shape: BoxShape.circle
            ),
            child: widget.icon ?? new Icon(Icons.add_rounded, color: Theme.of(context).backgroundColor, size: 20.0,),
          )
        ]
      ),
    );
  }
}
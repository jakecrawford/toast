
import 'package:flutter/material.dart';

class RadioRowButton extends StatefulWidget {
  final Function(bool) onChange;
  final bool selected;
  final String title;
  final EdgeInsets padding;

  RadioRowButton({
    @required this.onChange, @required this.selected, @required this.title, this.padding
  });

  @override
  _RadioRowButtonState createState() => _RadioRowButtonState();
}

class _RadioRowButtonState extends State<RadioRowButton> {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new FlatButton(
        padding: widget.padding ?? new EdgeInsets.all(10.0),
        minWidth: 20.0,
        onPressed: (){
          widget.onChange(!widget.selected);
          setState(() {
            
          });
        }, 
        child: new Row(
          children: [
            new Text(widget.title, style: Theme.of(context).textTheme.headline6,),
            new Container(width: 10.0,),
            new AnimatedContainer(
              duration: new Duration(milliseconds: 200),
              curve: Curves.ease,
              alignment: Alignment.center,
              width: 22.0, height: 22.0,
              decoration: new BoxDecoration(
                color: widget.selected ? Theme.of(context).primaryColor : Colors.transparent,
                shape: BoxShape.circle,
                border: new Border.all(
                  color: Theme.of(context).primaryColor,
                  width: 2.0
                )
              ),
              child: new AnimatedOpacity(
                opacity: widget.selected ? 1.0 : 0.0,
                duration: new Duration(milliseconds: 200),
                curve: Curves.ease,
                child: new Icon(Icons.check, color: Colors.white, size: 12.0,),
              )
            )
          ]
        ),
      ),
    );
  }
}
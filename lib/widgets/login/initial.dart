import 'package:flutter/material.dart';

class InitialPage extends StatefulWidget {
  final Function onSignUp;
  final Function onSignIn;

  InitialPage({
    @required this.onSignIn, @required this.onSignUp
  });

  @override
  _InitialPageState createState() => _InitialPageState();
}

class _InitialPageState extends State<InitialPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.width < 800.0 ? MediaQuery.of(context).size.height*0.6 : MediaQuery.of(context).size.height*0.5,
      child: new Stack(
        alignment: Alignment.center,
        children: [
          new Positioned(
            top: MediaQuery.of(context).size.height*0.05,
            child: new Container(
              child: new Image.asset('assets/logo@3x.png', height: 50.0,),
            ),
          ),
          new Positioned(
            bottom: 80.0,
            child: new Container(
              child: new Column(
                children: [
                  new FlatButton(
                    onPressed: (){
                      widget.onSignIn();
                    }, 
                    child: new Text("I already have an account",
                      style: Theme.of(context).textTheme.headline5,
                    )
                  ),
                  new Container(height: 20.0),
                  new RawChip(
                    onPressed: (){
                      widget.onSignUp();
                    },
                    backgroundColor: Theme.of(context).primaryColor,
                    label: new Container(
                      padding: new EdgeInsets.symmetric(
                        horizontal: 80.0, vertical: 10.0
                      ),
                      child: new Text('Sign Up',
                        style: Theme.of(context).textTheme.headline5,
                      ),
                    ),
                  )
                ]
              ),
            )
          )
        ]
      ),
    );
  }
}


import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast/textfield/flattext.dart';
import 'package:toast/widgets/buttons/chipButton.dart';

class LoginUser extends StatefulWidget {
  final Function onCancel;
  final Function(User) onLogin;

  LoginUser({
    @required this.onCancel, @required this.onLogin
  });

  @override
  _LoginUserState createState() => _LoginUserState();
}

class _LoginUserState extends State<LoginUser> {

  String email = "";
  String password = "";

  Widget error;

  login() async {
    try {
      User result = (await FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password)).user;
      if (User != null){
        widget.onLogin(result);
      }
    } catch(e){
      setState(() {
        error = new Text(e == null ? "Please enter valid information" : e?.message ?? "",
          style: new TextStyle(
            color: Theme.of(context).primaryColor,
            fontFamily: "Nunito",
            fontSize: 14.0
          ),
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
       height: MediaQuery.of(context).size.width < 800.0 ? MediaQuery.of(context).size.height*0.6 : MediaQuery.of(context).size.height*0.5,
      child: new Stack(
        alignment: Alignment.center,
        children: [
          MediaQuery.of(context).size.width < 800.0 ? new Container() : new Positioned(
            top: MediaQuery.of(context).size.height*0.05,
            child: new Container(
              child: new Image.asset('assets/logo@3x.png', height: 50.0,),
            ),
          ),
          new Padding(
            padding: new EdgeInsets.symmetric(horizontal: 50.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                new Text("Account Login", style: Theme.of(context).textTheme.headline4,),
                new FlatTextField(
                  autoFocus: true,
                  textInputType: TextInputType.emailAddress,
                  textCapitalization: TextCapitalization.none,
                  autoCorrect: false,
                  inputDecoration: new InputDecoration(
                    hintStyle: Theme.of(context).textTheme.bodyText2,
                    hintText: "Email",
                  ),
                  onChange: (v){
                    email = v;
                  }
                ),
                new FlatTextField(
                  textInputType: TextInputType.visiblePassword,
                  textCapitalization: TextCapitalization.none,
                  autoCorrect: false,
                  inputDecoration: new InputDecoration(
                    hintStyle: Theme.of(context).textTheme.bodyText2,
                    hintText: "Password",
                  ),
                  onChange: (v){
                    password = v;
                  }
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 20.0),
                  child: error ?? new Container(),
                ),
                new Container(height: 100.0,)
              ]
            ),
          ),
          new Positioned(
            bottom: 80.0,
            child: new Column(
              children: [
                new FlatButton(onPressed: (){
                  FocusScope.of(context).requestFocus(new FocusNode());
                  widget.onCancel();
                }, child: new Text("Cancel", style: Theme.of(context).textTheme.headline5,)),
                new Container(height: 20.0,),
                new ChipButton(onTap: (){
                  login();
                }, text: "Login")
              ]
            )
          )
        ],
      )
    );
  }
}
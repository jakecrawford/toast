
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:toast/repositories/user.dart';
import 'package:toast/toast/textfield/flattext.dart';
import 'package:toast/widgets/buttons/chipButton.dart';

class SignUpUser extends StatefulWidget {
  final Function onCancel;
  final Function(User) onLogin;

  SignUpUser({
    @required this.onCancel, @required this.onLogin
  });

  @override
  _SignUpUserState createState() => _SignUpUserState();
}

class _SignUpUserState extends State<SignUpUser> {

  String email = "";
  String password = "";
  String name = "";

  Widget error;
  String message = "";

  signUp() async {
    if (email != "" && password != "" && name != ""){
      try {
        UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: email,
          password: password
        );
        User user = FirebaseAuth.instance.currentUser;
        UserRepository.createNewUser(name);
        widget.onLogin(userCredential.user);
      } on FirebaseAuthException catch (e) {
        print(e.toString());
        if (e.code == 'weak-password') {
          message = "The password provided is too weak.";
        } else if (e.code == 'email-already-in-use') {
          message = "The account already exists for that email.";
        }
      } catch (e) {
        message = e.toString();
      }
    } else {
      message = "Please complete all information";
    }
    if (mounted){
      setState(() {
        error = new Text(message,
          style: new TextStyle(
            color: Theme.of(context).primaryColor,
            fontFamily: "Nunito",
            fontSize: 14.0
          ),
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
       height: MediaQuery.of(context).size.width < 800.0 ? MediaQuery.of(context).size.height*0.6 : MediaQuery.of(context).size.height*0.5,
      child: new Stack(
        alignment: Alignment.center,
        children: [
          MediaQuery.of(context).size.width < 800.0 ? new Container() : new Positioned(
            top: MediaQuery.of(context).size.height*0.05,
            child: new Container(
              child: new Image.asset('assets/logo@3x.png', height: 50.0,),
            ),
          ),
          new Padding(
            padding: new EdgeInsets.symmetric(horizontal: 50.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                 new Text("Create Account", style: Theme.of(context).textTheme.headline4,),
                 new FlatTextField(
                  autoFocus: true,
                  textInputType: TextInputType.name,
                  inputDecoration: new InputDecoration(
                    hintStyle: Theme.of(context).textTheme.bodyText2,
                    hintText: "Username",
                  ),
                  onChange: (v){
                    name = v;
                  }
                ),
                new FlatTextField(
                  textInputType: TextInputType.emailAddress,
                  textCapitalization: TextCapitalization.none,
                  autoCorrect: false,
                  inputDecoration: new InputDecoration(
                    hintStyle: Theme.of(context).textTheme.bodyText2,
                    hintText: "Email",
                  ),
                  onChange: (v){
                    email = v;
                  }
                ),
                new FlatTextField(
                  textInputType: TextInputType.visiblePassword,
                  textCapitalization: TextCapitalization.none,
                  autoCorrect: false,
                  inputDecoration: new InputDecoration(
                    hintStyle: Theme.of(context).textTheme.bodyText2,
                    hintText: "Password",
                  ),
                  onChange: (v){
                    password = v;
                  }
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 20.0),
                  child: new AnimatedContainer(duration: new Duration(seconds: 1),
                    child: error ?? new Container(),
                  )
                ),
                new Container(height: 100.0,)
              ]
            ),
          ),
          new Positioned(
            bottom: 80.0,
            child: new Column(
              children: [
                new FlatButton(onPressed: (){
                  FocusScope.of(context).requestFocus(new FocusNode());
                  widget.onCancel();
                }, child: new Text("Cancel", style: Theme.of(context).textTheme.headline5,)),
                new Container(height: 20.0,),
                new ChipButton(onTap: (){
                  signUp();
                }, text: "Sign Up")
              ]
            )
          )
        ],
      )
    );
  }
}
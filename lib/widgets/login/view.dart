
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:toast/views/dashboard.dart';
import 'package:toast/widgets/login/initial.dart';
import 'package:toast/widgets/login/login.dart';
import 'package:toast/widgets/login/signup.dart';

class LoginPage extends StatefulWidget {
  final Widget background;
  final Function onLogin;

  LoginPage({
    @required this.background, @required this.onLogin,
  });

  @override
  _LoginPageState createState() => _LoginPageState();
}

enum LoginState {
  none, login, signup,
}

class _LoginPageState extends State<LoginPage> {

  LoginState state = LoginState.none;

  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height,
        child: new Stack(
          children: [
            widget.background,
            new SingleChildScrollView(
              physics: AlwaysScrollableScrollPhysics(),
              child: new Container(
                width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height,
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    new Container(height: MediaQuery.of(context).size.height*0.2),
                    new Container(
                      width: MediaQuery.of(context).size.width < 800.0 ? MediaQuery.of(context).size.width : MediaQuery.of(context).size.width*0.6, 
                      decoration: new BoxDecoration(
                        color: Theme.of(context).backgroundColor,
                        borderRadius: new BorderRadius.all(Radius.circular(10.0)),
                      ),
                      margin: MediaQuery.of(context).size.width < 800.0 ? new EdgeInsets.symmetric(horizontal: 20.0) : new EdgeInsets.all(0.0),
                      child: state == LoginState.none ? 
                        new InitialPage(
                          onSignIn: (){
                            setState(() {
                              state = LoginState.login;
                            });
                          },
                          onSignUp: (){
                            setState(() {
                              state = LoginState.signup;
                            });
                          },
                        ): state == LoginState.login ? 
                        new LoginUser(
                          onCancel: (){
                            setState(() {
                              state = LoginState.none;
                            });
                          },
                          onLogin: (User u){
                            widget.onLogin();
                          },
                        ):
                        new SignUpUser(
                          onCancel: (){
                            setState(() {
                              state = LoginState.none;
                            });
                          },
                          onLogin: (User v){
                            widget.onLogin();
                          },
                        )
                    ),
                  ],
                ),
              )
        
            )
          ]
        )
      ),
    );
  }
}
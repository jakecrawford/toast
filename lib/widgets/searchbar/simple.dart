
import 'package:flutter/material.dart';
import 'package:toast/toast/textfield/flattext.dart';

class SimpleSearchBar extends StatefulWidget {
  final Function(String) onChange;
  final Function(String) onConfirm;
  SimpleSearchBar({
    this.onChange, this.onConfirm,
  });
  @override
  _SimpleSearchBarState createState() => _SimpleSearchBarState();
}

class _SimpleSearchBarState extends State<SimpleSearchBar> {

  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        new Container(
          margin: new EdgeInsets.only(left: 20.0, right: 20.0),
          decoration: new BoxDecoration(
            borderRadius: new BorderRadius.all(Radius.circular(50.0)),
            color: Theme.of(context).cardColor
          ),
          width: double.infinity, height: 48.0,
          alignment: Alignment.centerLeft,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new Padding(
                padding: new EdgeInsets.only(left: 15.0, right: 10.0),
                child: new Icon(Icons.search_rounded),
              ),
              new Expanded(
                child: new FlatTextField(
                  hint: "Search for users",
                  onChange: (v){
                    widget?.onChange(v);
                  },
                  onSubmit: (v){
                    widget?.onChange(v);
                    widget?.onConfirm(v);
                  },
                  inputDecoration: new InputDecoration(
                    hintStyle: Theme.of(context).textTheme.headline5,
                    hintText: "Search for friends",
                    border: new UnderlineInputBorder(
                      borderSide: BorderSide.none,
                    )
                  ),
                ),
              ),
              new RawChip(
                onPressed: (){
       
                },
                backgroundColor: Theme.of(context).primaryColor,
                label: new Container(
                  child: new Text("SEARCH",
                    style: Theme.of(context).textTheme.headline5,
                  ),
                ),
              ),
              new Container(width: 10.0,)
            ],
          )
        )
      ],
    );
  }
}

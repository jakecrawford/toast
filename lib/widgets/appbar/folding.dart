import 'package:flutter/material.dart';
import 'dart:math' as math;

class FoldingAppBarDelegate extends SliverPersistentHeaderDelegate {
  
  FoldingAppBarDelegate({
    @required this.child,
    this.height = 103.0,
  });

  final double height;
  final Widget child;
   
  @override double get minExtent => height;
  @override double get maxExtent => math.max(0.0, minExtent);

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
   
    return new SizedBox.expand(
      child: child
    );
  }

	@override
  bool shouldRebuild(FoldingAppBarDelegate oldDelegate) {
    return true;
  }
}
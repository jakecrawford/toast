import 'package:flutter/material.dart';

class SplashSelector extends StatefulWidget {
  final List<String> items;
  final PageController pageController;
  final Color color;

  SplashSelector({
    @required this.pageController, @required this.items, this.color
  });

  @override
  _SplashSelectorState createState() => _SplashSelectorState();
}

class _SplashSelectorState extends State<SplashSelector> {

  int selected = 0;

  update(){
    try {
      selected = (widget.pageController.page).round();
      setState(() {
        
      });
    } catch(e){

    }
  }

  @override
  void initState() {
    widget.pageController.addListener(update);

    super.initState();
  }

  @override
  void dispose() {
    widget.pageController?.removeListener(update);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: MediaQuery.of(context).size.width < 800.0 ? new EdgeInsets.all(0.0) : new EdgeInsets.only(left: 10.0, right: 10.0),
      child: new SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: List.generate(widget.items.length, (index){
            return new GestureDetector(
              onTap: (){
                setState(() {
                  widget.pageController.animateToPage(
                    index, duration: new Duration(milliseconds: 200), 
                    curve: Curves.easeOutQuart
                  );
                });
              },
              child: new Container(
                color: Colors.transparent,
                margin: MediaQuery.of(context).size.width < 800.0 && index == 0 ? new EdgeInsets.only(left: 10.0) : new EdgeInsets.all(0.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new AnimatedContainer(
                      duration: new Duration(milliseconds: 200),
                      curve: Curves.ease,
                      decoration: selected == index ? new BoxDecoration(
                        color: widget.color ?? Theme.of(context).primaryColor,
                        borderRadius: new BorderRadius.all(Radius.circular(25.0))
                      ): new BoxDecoration(),
                      child: new Container(
                        margin: new EdgeInsets.symmetric(horizontal: 20.0, vertical: 6.0),
                        child: new Text(widget.items[index], style: Theme.of(context).textTheme.headline5),
                      )
                    )
                  ]
                )
              ),
            );
          }),
        ),
      )
    );
  }
}
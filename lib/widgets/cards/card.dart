import 'package:flutter/material.dart';

class FloatingCard extends StatefulWidget {
  final Widget child;
  FloatingCard({@required this.child});

  @override
  _FloatingCardState createState() => _FloatingCardState();
}

class _FloatingCardState extends State<FloatingCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: new BorderRadius.all(Radius.circular(12.0)),
        boxShadow: <BoxShadow>[
          new BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.2),
            blurRadius: 9.0, spreadRadius: 9.0, offset: new Offset(0.0, 9.0)
          )
        ]
      ),
      child: widget.child,
    );
  }
}